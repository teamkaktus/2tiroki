<?php
// Form
$_['quick_order'] = 'Укажите ваше имя и номер телефона, наши специалисты свяжутся с вами для уточнения деталей заказа';
$_['name'] = 'Имя';
$_['phone'] = 'Телефон';
$_['email'] = 'E-Mail';
$_['comment'] = 'Комментарий';
$_['checkout'] = 'Отправить';
$_['continue_shopping'] = 'Продолжить покупки';

// Product Cart Table
$_['product_title'] = 'Наименование';
$_['product_qty'] = 'Кол-во';
$_['product_price'] = 'Цена';
$_['product_cost'] = 'Стоимость';

// Cart Action
$_['remove'] = 'Удалить';
$_['from_cart'] = 'из корзины';

// Success Order
$_['order_num'] = 'Спасибо!';
$_['successful_sended'] = 'успешно оформлен!';
$_['thanks_for_order'] = 'Ваша заявка принята, ожидайте наши специалисты свяжутся с вами в ближайшее время!';
?>