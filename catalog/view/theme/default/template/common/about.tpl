<?php echo $header; ?>
<section id="page-title" style="background-image: url(image/catalog/bg-large.jpg);">
  <div class="container">
    <div class="row">
      <h3>cпециализированный магазин</h3>
      <h2>Умных часов для детей</h2>
    </div>
  </div>
</section>

<section id="breadcrumbs">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <Ul>
          <li><a href="">Главная</a></li>
          <li>О компании</li>
        </Ul>
      </div>
    </div>
  </div>
</section>

<div class="content">
  <div class="container">
    <h2 class="h2-page-title">О компании</h2>
    <div class="row">
      <div class="col-md-6 mb">
        <img src="image/catalog/about-img-new.png" alt="" class="img-responsive">
      </div>
      <div class="col-md-6">
        <div class="page-descr contacts-page-descr">
          <h2>ООО "Умный Гаджет"</h2>
          <span class="contact-inf"><span class="bold">ОГРН:</span>5167746341362</span>
          <span class="contact-inf"><span class="bold">ИНН</span>7716839992</span>
          <span class="contact-inf"><span class="bold">КПП:</span>771601001</span>
          <span class="contact-inf"><span class="bold">Расчетный счет:</span>40702810001100010376</span>
          <span class="contact-inf"><span class="bold">Банк:</span>АО "АЛЬФА-БАНК" г.Москва</span>
          <span class="contact-inf"><span class="bold">Кор.счет:</span>30101810200000000593</span>
          <span class="contact-inf"><span class="bold">БИК банка:</span>044525593</span>
          <span class="about-inf"><span class="bold blue">Адрес магазина:</span> Москва, Носовихинское шоссе 45. ТРЦ Реутов Парк 2 этаж, павильон напротив "Спортмастер"</span>
          <div class="work-time">
            <div class="left-block">
              <span class="bl-tit"><span class="blue">Часы ​работы магазина</span><br>с 10:00 до 23:00</span><br>
              <span class="bl-tit"><span class="blue">Часы работы интернет магазина </span><br>с 10:00 до 19:00</span>
            </div>
            <div class="right-block">
              <div class="contacts">
                <img src="image/catalog/phone-small.png" class="img-responsive" alt="">
                <span>7(495)104-97-77</span>
              </div>
              <div class="contacts">
                <img src="image/catalog/mail-small.png" class="img-responsive" alt="">
                <span>info.tiroki@gmail.com​</span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<section class="common-cont">
  <div class="container">
    <div class="row">
      <div class="com-cont">
        <div class="row">
          <div class="col-md-4">
            <div class="com-wrap">
              <img src="image/catalog/zayavka.png" alt="">
              <span>Прямой договор с фабрикой производителем.</span>
            </div>
          </div>
          <div class="col-md-4">
            <div class="com-wrap">
              <img src="image/catalog/bumz.png" alt="">
              <span>Предлагаем лучшие цены на рынке</span>
            </div>
          </div>
          <div class="col-md-4">
            <div class="com-wrap">
              <img src="image/catalog/kluc.png" alt="">
              <span>Работает сервисный центр в Москве</span>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-4">
            <div class="com-wrap">
              <img src="image/catalog/gruz.png" alt="">
              <span>Доставляем собственной курьерской службой</span>
            </div>
          </div>
          <div class="col-md-4">
            <div class="com-wrap">
              <img src="image/catalog/smile.png" alt="">
              <span>5 лет на рынке - тысячи довольных клиентов</span>
            </div>
          </div>
          <div class="col-md-4">
            <div class="com-wrap">
              <img src="image/catalog/rainbow.png" alt="">
              <span>Лучшая компания года по версии агентства Rainbow</span>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="common-cont">
  <div class="container">
    <div class="row">
      <div class="com-cont">
        <div class="row">
          <div class="col-md-12">
            <h4 class="partners">Наши партенры</h4>
          </div>
        </div>
        <div class="row partners-row">
          <div class="col-md-2">
            <a href="#"><img src="image/catalog/cdek.png" class="img-responsive" alt=""></a>
          </div>
          <div class="col-md-2">
            <a href="#"><img src="image/catalog/post.png" class="img-responsive" alt=""></a>
          </div>
          <div class="col-md-2">
            <a href="#"><img src="image/catalog/poisk.png" class="img-responsive" alt=""></a>
          </div>
          <div class="col-md-2">
            <a href="#"><img src="image/catalog/evro.png" class="img-responsive" alt=""></a>
          </div>
          <div class="col-md-2">
            <a href="#"><img src="image/catalog/megafon.png" class="img-responsive" alt=""></a>
          </div>
          <div class="col-md-2">
            <a href="#"><img src="image/catalog/eld.png" class="img-responsive" alt=""></a>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!--<div class="container">
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?><?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>-->
<?php echo $footer; ?>