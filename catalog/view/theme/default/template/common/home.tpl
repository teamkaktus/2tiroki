<?php echo $header; ?>
<div class="page">
    <div class="banner" style="background-image: url(/image/catalog/bg.png);">
        <div class="container">
            <div class="row">
                <h2>cпециализированный магазин</h2>
                <h1>Умных часов<br>для детей</h1>
                <a href="/index.php?route=product/catalog" class="button">Перейти в каталог</a>
                <img class="boy img-responsive" src="/image/catalog/boy.png" alt="">
                <img class="clocks img-responsive" src="/image/catalog/clocks.png" alt="">
            </div>
        </div>
    </div>
    <section id="whywe" style="background-image: url(/image/catalog/bg-2.png);">
        <div class="container">
            <div class="row">
                <h2 class="sec-title">
                    Почему родители
                </h2>
                <h3 class="sec-subtitle">
                    выбирают нас
                </h3>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <div class="whywe-wrap">
                        <img src="/image/catalog/feat-1.png" alt="">
                        <h4>Достойное качество - сертификат ГОСТ!</h4>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="whywe-wrap">
                        <img src="/image/catalog/feat-2.png" alt="">
                        <h4>Цены от производителя - постоянные акции!</h4>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="whywe-wrap">
                        <img src="/image/catalog/feat-3.png" alt="">
                        <h4>Сервисное обслуживание - совершенно бесплатно!</h4>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="whywe-wrap">
                        <img src="/image/catalog/feat-4.png" alt="">
                        <h4>Техническая поддержка 24/7</h4>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <?php echo $column_left; ?>
                </div>
                <div class="col-md-9">
                    <section class="cont-title">
                        <h2>Детские умные часы Tiroki</h2>
                        <p>Детские часы-телефон с GPS-трекером – инновационное изобретение, полюбившееся потребителям
                            всего мира. Родители избавляются от проблем, имея возможность отслеживать перемещения
                            ребенка в режиме онлайн. Дети получают современный гаджет, снабженный мощными возможностями
                            смартфона или планшета. Купить детские умные часы по цене изготовителя с гарантией и
                            доставкой в любую точку страны можно в нашем интернет-магазине.</p>
                    </section>
                    <section class="content-section">
                        <div class="row-title hidden-xs">Новинки</div>
                        <div class="row-title hidden-lg hidden-md hidden-sm">Для мальчиков в черном</div>
                        <?php echo $content_bottom; ?>
                        <div class="row-title hidden-lg hidden-md hidden-sm">Для девочек в розовом</div>
                        <?php echo $content_center; ?>
                        <div style="clear: both"></div>

                        <section class="cont-title">
                            <h2>Принцип работы умных часов</h2>
                            <p>На первый взгляд, ТИРОКИ – яркие стильные часы с безукоризненно точным ходом,
                                будильником, шагомером, секундомером, календарем. Ребенок не проспит подъем в детский
                                сад или школу! Но это и мощное коммуникационное устройство, дающее возможность
                                отправлять голосовые и текстовые сообщения, выходить в Сеть, оставаться на связи с
                                родителями и друзьями. Вставленная сим-карта формата micro и встроенная геолокация
                                позволяет взрослым следить за перемещениями детей и не волноваться за местонахождение.
                                Как активировать работу часов?</p>
                            <ul>
                                <li>Покупаете часы в нашем магазине, вставляете сим-карту в гаджет малыша.</li>
                                <li>Скачиваете мобильное приложение из Play Market или App Store.</li>
                                <li>Синхронизируете сотовый номер ребенка с данными приложения.</li>
                                <li>Вносите нужные контакты в телефонную книгу ТИРОКИ.</li>
                                <li>Теперь ребенок всегда будет в зоне внимания и контроля!</li>
                            </ul>
                        </section>
                        <div style="clear: both"></div>
                        <div class="row-title hidden-lg hidden-md hidden-sm">Для самых ярких</div>
                        <div class="row-title hidden-xs">Лучшие предложения</div>
                        <?php echo $content_top; ?>
                        <div style="clear: both"></div>
                        <section class="cont-title">
                            <h2>Уникальные возможности</h2>
                            <p>Голосовая связь, подключение к интернету – далеко не все возможности ТИРОКИ. Купить Smart
                                Baby Watch в Москве – это значит оградить ребенка от опасностей большого города, которые
                                подстерегают подростков на каждом шагу. Не секрет, что ежедневно в России пропадает
                                более 50 малышей, еще больше общается с незнакомыми людьми: именно через детей многие
                                квартирные воры выслеживают потенциальных жертв. Благодаря удивительным особенностям
                                умных часов опасность попасть в группу риска гораздо меньше.</p>
                            <ul>
                                <li>Создайте безопасную зону: когда ребенок выйдет за ее пределы, вам поступит
                                    уведомление.
                                </li>
                                <li>Будьте в курсе того, с кем общается сын или дочь, достаточно включить
                                    аудио-мониторинг.
                                </li>
                                <li>Отправляйте смс-сообщения и звоните малышу прямо на часы: дорогой смартфон не
                                    нужен!
                                </li>
                                <li>Следите за перемещениями ребенка из любого уголка земного шара, история
                                    сохраняется.
                                </li>
                                <li>Корректируйте маршрут малыша в режиме реального времени, оставайтесь на связи
                                    всегда.
                                </li>
                            </ul>
                        </section>
                        <div style="clear: both"></div>
                        <div class="row-title hidden-xs">Популярные товары</div>
                        <div class="row-title hidden-lg hidden-md hidden-sm">Классика</div>
                        <?php echo $column_right; ?>


                        <div style="clear: both"></div>
                        <div class="back-call" style="background-image: url(/image/catalog/cat-bg.png);">

                            <form id="oContact">
                                <label for="name">Обратный<br>звонок</label>
                                <input type="text" id="name" name="oName" placeholder="Ваше имя">
                                <input type="text" id="phone" name="oPhone" placeholder="Ваш телефон">
                                <input type="submit" id="oSend" value="Заказать">
                            </form>
                        </div>

                        <section class="cont-title">
                            <h2>Кому нужны часы ТИРОКИ?</h2>
                            <p>Ответ однозначен – всем мальчикам и девочкам, вынужденным проводить значительное время
                                без присмотра взрослых. Гаджет необходим родителям: о волнениях по поводу нахождения
                                малыша можно забыть. Девайс нужен ребенку: стильный аксессуар станет объектом
                                пристального внимания друзей и одноклассников. Остерегайтесь подделок: купить часы
                                детские с сим-картой и GPS-трекером следует только в специализированном магазине. Мы
                                являемся официальными дилерами компании-изготовителя, поэтому у нас вы купите настоящие
                                ТИРОКИ!</p>
                            <ul>
                                <li>Всегда в наличии большой выбор разноцветных моделей для мальчиков и девочек.
                                </li>
                                <li>В продаже только настоящие часы, имеющие сертификат качества.
                                </li>
                                <li>Поможем настроить устройство, объясним, как им пользоваться.
                                </li>
                                <li>Осуществляется курьерская доставка по Москве и области, отправляем заказы в регионы.
                                </li>
                                <li>Есть возможность обмена или возврата при наличии брака, действует годовая гарантия.
                                </li>
                            </ul>
                            <p></p>
                            <p>Вследствие прямых договоров между фабрикой и магазином мы имеем возможность
                                предлагать уникальный гаджет по цене производителя. Стоимость умных часов в нашей
                                компании ниже средних цен по Москве на 7–10%. Купив Smart Baby Watch, вы не только
                                защитите ребенка от соблазнов мегаполиса, но и сделаете малышу прекрасный подарок.
                                Умные часы ТИРОКИ – надежное средство связи и модный аксессуар, который наверняка
                                понравится ребенку любого возраста!</p>
                        </section>

                    </section>
                </div>
            </div>
        </div>
    </div>
    <section id="feedbacks">
        <div class="container">
            <h2>Отзывы счастливых покупателей</h2>
            <div class="row">
                <div class="col-md-12">
                    <div class="feeds-wrap owl-carousel hidden-xs">
                        <div class="feed">
                            <div class="lazyYT" data-youtube-id="hnB5aGW9AaU" data-ratio="16:9">1</div>
                        </div>
                        <div class="feed">
                            <div class="lazyYT" data-youtube-id="QeXberdrtws" data-ratio="16:9">1</div>
                        </div>
                        <div class="feed">
                            <div class="lazyYT" data-youtube-id="bNhxOKF8gys" data-ratio="16:9">1</div>
                        </div>
                        <div class="feed">
                            <div class="lazyYT" data-youtube-id="wxHz2mWs_ZI" data-ratio="16:9">1</div>
                        </div>
                        <div class="feed">
                            <div class="lazyYT" data-youtube-id="1RwmGahvubA" data-ratio="16:9">1</div>
                        </div>
                        <div class="feed">
                            <div class="lazyYT" data-youtube-id="qmbFuLLc-Mg" data-ratio="16:9">1</div>
                        </div>
                        <div class="feed">
                            <div class="lazyYT" data-youtube-id="vm92lJtNi0A" data-ratio="16:9">1</div>
                        </div>
                    </div>

                    <div class="feeds-accordion visible-xs">
                        <div class="feed">
                            <div class="lazyYT" data-youtube-id="hnB5aGW9AaU" data-ratio="16:9">1</div>
                        </div>
                        <div class="feed-content">
                            <div class="feed">
                                <div class="lazyYT" data-youtube-id="QeXberdrtws" data-ratio="16:9">1</div>
                            </div>
                            <div class="feed">
                                <div class="lazyYT" data-youtube-id="bNhxOKF8gys" data-ratio="16:9">1</div>
                            </div>
                            <div class="feed">
                                <div class="lazyYT" data-youtube-id="wxHz2mWs_ZI" data-ratio="16:9">1</div>
                            </div>
                            <div class="feed">
                                <div class="lazyYT" data-youtube-id="1RwmGahvubA" data-ratio="16:9">1</div>
                            </div>
                            <div class="feed">
                                <div class="lazyYT" data-youtube-id="qmbFuLLc-Mg" data-ratio="16:9">1</div>
                            </div>
                            <div class="feed">
                                <div class="lazyYT" data-youtube-id="vm92lJtNi0A" data-ratio="16:9">1</div>
                            </div>
                        </div>
                        <a href="#" class="all-feeds button center-block visible-xs">Все отзывы</a>
                    </div>

                    <div class="feeds-nav hidden-xs">
                        <a href="#" class="feeds-btn feeds-prev"><i class="fa fa-chevron-left"></i></a>
                        <a href="#" class="feeds-btn feeds-next"><i class="fa fa-chevron-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php echo $footer; ?>
<script type="text/javascript">
$(function(){
    $("#phone").mask("+7 (999) 999-99-99");
});
</script>
<script>
      $(document).ready(function() {
         $('#oSend').on('click', function(e){  
            e.preventDefault();      
            var res = $('#oContact').serializeArray();
            var arr = {};
            $.each(res, function (result) {
                 var $index = res[result].name;
                 arr[$index] = res[result].value;
            });
            $.ajax({
                url: 'index.php?route=common/wholesalers/oContact',
                type: 'post',
                dataType: 'json',
                data: arr,
                success: function () {
                }
            });
            $('#name').val('');
            $('#phone').val('');
            swal("Сообщение отправлено", "", "success");
         });
      });
</script>