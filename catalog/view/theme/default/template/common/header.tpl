<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]>
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]>
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<!--<![endif]-->
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo $title; if (isset($_GET['page'])) { echo " - ". ((int) $_GET['page'])." ".$text_page;} ?></title>
    <base href="<?php echo $base; ?>"/>
    <?php if ($description) { ?>
    <meta name="description" content="<?php echo $description; if (isset($_GET['page'])) { echo " -
    ". ((int) $_GET['page'])." ".$text_page;} ?>" />
    <?php } ?>
    <?php if ($keywords) { ?>
    <meta name="keywords" content="<?php echo $keywords; ?>"/>
    <?php } ?>
    <meta property="og:title" content="<?php echo $title; if (isset($_GET['page'])) { echo " -
    ". ((int) $_GET['page'])." ".$text_page;} ?>" />
    <meta property="og:type" content="website"/>
    <meta property="og:url" content="<?php echo $og_url; ?>"/>
    <?php if ($og_image) { ?>
    <meta property="og:image" content="<?php echo $og_image; ?>"/>
    <?php } else { ?>
    <meta property="og:image" content="<?php echo $logo; ?>"/>
    <?php } ?>
    <meta property="og:site_name" content="<?php echo $name; ?>"/>
    <script src="catalog/view/javascript/jquery/jquery-2.1.1.min.js" type="text/javascript"></script>
    <!--<link href="catalog/view/javascript/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen"/>-->
    
    <link href="catalog/view/javascript/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <link href="//fonts.googleapis.com/css?family=Open+Sans:400,400i,300,700" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" type="text/css"
          href="catalog/view/stylesheet/pdqo/vendor/magnific-popup/magnific-popup.min.css"/>
    <link rel="stylesheet" type="text/css" href="catalog/view/stylesheet/pdqo/vendor/animate/animate.min.css"/>
    <link rel="stylesheet" type="text/css" href="catalog/view/stylesheet/pdqo/pdqo.min.css"/>
    <link rel="stylesheet" type="text/css" href="catalog/view/stylesheet/pdqo/default.css"/>
    <link href="catalog/view/theme/default/stylesheet/stylesheet.css" rel="stylesheet">
    <link href="catalog/view/theme/default/stylesheet/main.min.css" rel="stylesheet">
    
    <link href="/jivosite/jivosite.css" rel="stylesheet">
    <script src="/jivosite/jivosite.js" type="text/javascript"></script>
    
    <?php foreach ($styles as $style) { ?>
    <link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>"
          media="<?php echo $style['media']; ?>"/>
    <?php } ?>
    <?php foreach ($scripts as $script) { ?>
    <script src="<?php echo $script; ?>" type="text/javascript"></script>
    <?php } ?>
    <script src="catalog/view/javascript/common.js" type="text/javascript"></script>
    <?php foreach ($links as $link) { ?>
    <link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>"/>
    <?php } ?>
    
    <?php foreach ($analytics as $analytic) { ?>
    <?php echo $analytic; ?>
    <?php } ?>
</head>
<body class="<?php echo $class; ?>">
  <img src="/image/catalog/bg_opt_zayavka.jpg" style="display: none">
<div class="page">
  <header>
      <div class="top-line">
          <div class="container">
              <div class="row visible-md visible-lg">
                  <nav class="menu" id="main-menu">
                      <ul>
                          <li><a href="/">Главная</a></li>
                          <li><a href="/index.php?route=product/catalog">Каталог</a></li>
                          <li><a href="/index.php?route=common/delivery">Доставка и оплата</a></li>
                          <li><a href="/index.php?route=common/about">о компании</a></li>
                          <li><a href="/index.php?route=common/guarantees">Гарантии</a></li>
                          <li><a href="/index.php?route=common/wholesalers">Оптовикам</a></li>
                          <li><a href="/index.php?route=common/aboutClock">Что такое умные часы <span
                                          class="upcs">tiroki</span></a></li>
                          <li><a href="/index.php?route=information/contact">Контакты</a></li>
                      </ul>
                  </nav>
              </div>
          </div>
          <div class="left hidden-md hidden-lg">
              <a href="#mobile-menu" class="hamburger hamburger--slider visible-xs visible-sm"><span
                          class="hamburger-box"><span class="hamburger-inner"></span></span></a>
              <span class="menu-text upcs">Меню</span>
          </div>
          <div class="right hidden-md hidden-lg">
              <img src="image/catalog/phone.png" alt="" class="img-responsive">
              <a href="tel:<?php echo $telephone; ?>" class="phone-num"><?php echo $telephone; ?></a>
              <span class="call-free">Звонок по России бесплатный</span>
          </div>
      </div>
      <div class="container">
          <div class="row">
              <div class="logo-line">
                  <a href=""><img src="<?php echo $logo; ?>" class="img-responsive logo-img" alt=""><span
                              class="logo-text">гарантия<br>безопасности!</span></a>
                  <?php echo $search; ?>
                  <div class="phone-wrap hidden-sm hidden-xs">
                      <img src="image/catalog/phone.png" alt="" class="img-responsive">
                      <a href="tel:<?php echo $telephone; ?>" class="phone-num"><?php echo $telephone; ?></a>
                      <span class="call-free">Звонок по России бесплатный</span>
                  </div>

                  <?php echo $cart; ?>
              </div>
          </div>
      </div>
  </header>
