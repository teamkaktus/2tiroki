<div id="cart">
  <a href="/index.php?route=checkout/cart" class="cart hidden-sm hidden-xs"><img
            src="image/catalog/cart.png" alt=""><span>Корзина</span><span
            class="badge"><?php echo $text_items; ?></span>
  </a>
  <a class="cart cart-popup-btn hidden-md hidden-lg cart_mob_wee" href="/index.php?route=checkout/cart"><img
            src="image/catalog/cart-mob.png"
            class="img-responsive" alt=""><span
            class="badggee"><?php echo $text_items; ?></span>
  </a>
</div>
