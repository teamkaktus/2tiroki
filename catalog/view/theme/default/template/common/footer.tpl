<form>
  
</form>
<section id="callback">
  <div class="container">
    <h2>Задать вопрос</h2>
    <div class="row">
      <div class="col-md-9">
        <form id="qContact" action="#" class="callback-form">
          <div class="input-block">
            <input type="text" name="qName" placeholder="Ваше имя" id="callback-name">
            <input type="email" name="qEmail" placeholder="Ваша почта" id="callback-email">
          </div>
          <textarea placeholder="Сообщение" name="qText" rows="4" id="callback-text"></textarea>
          <div class="clearfix"></div>
          <p>Мы отправим ответ на Вашу электронную почту.</p>
          <input id="qSend" type="submit" value="Отправить" class="button">
        </form>
      </div>
      <div class="col-md-3 hidden-xs hidden-sm">
        <img src="image/catalog/map.png" class="img-responsive" alt="">
      </div>
    </div>
  </div>
</section>


<footer>
  <div class="container">
  <div class="row">
    <div class="col-md-3 hidden-xs hidden-sm">
      <a class="footer-logo" href=""><img src="image/catalog/logo.png" class="img-responsive" alt=""><span class="logo-text">гарантия<br>безопасности!</span></a>
      <span class="flab hidden-xs hidden-sm">© Tiroki,2017</span>
    </div>
    <div class="col-md-3 col-xs-6">
      <ul>
        <li><a href="/">Главная</a></li>
        <li><a href="/index.php?route=product/catalog">Каталог</a></li>
        <li><a href="/index.php?route=common/delivery">Доставка и оплата</a></li>
        <li><a href="/index.php?route=common/about">о компании</a></li>
        <li><a href="/index.php?route=common/guarantees">Гарантии</a></li>
        <li><a href="/index.php?route=common/wholesalers">Оптовикам</a></li>
        <li><a href="/index.php?route=common/aboutClock">Что такое умные часы <span class="upcs">tiroki</span></a></li>
        <li><a href="/index.php?route=information/contact">Контакты</a></li>
      </ul>
    </div>
    <div class="col-md-3 col-xs-6">
      <ul>
        <li>Мы в инстаграмм <a href="https://www.instagram.com/originalbabywatch/"><img src="image/catalog/insta-ico.png" class="inst" alt=""></a></li>
      </ul>
      <div class="cards">
        <h3>Мы принимаем к оплате:</h3>
        <div class="cards-images">
          <a href="#"><img src="image/catalog/yandex.png" alt="" class="img-responsive"></a>
          <a href="#"><img src="image/catalog/webmoney.png" alt="" class="img-responsive"></a>
          <a href="#"><img src="image/catalog/qiwi.png" alt="" class="img-responsive"></a>
          <a href="#"><img src="image/catalog/visa.png" alt="" class="img-responsive"></a>
          <a href="#"><img src="image/catalog/master_card.png" alt="" class="img-responsive"></a>
        </div>
      </div>
    </div>
    <div class="col-md-3 hidden-xs hidden-sm">
      <div class="phone-wrap">
        <img src="image/catalog/phone.png" alt="" class="img-responsive">
        <a href="tel:88002004146" class="phone-num">8 800 200 41 46</a>
        <span class="call-free">Звонок по России бесплатный</span>
      </div>
    </div>
    <div class="col-xs-12 hidden-md hidden-lg">
      <a class="footer-logo" href=""><img src="image/catalog/logo.png" class="img-responsive" alt=""></a>
      <div class="phone-wrap">
        <img src="image/catalog/phone.png" alt="" class="img-responsive">
        <a href="tel:88002004146" class="phone-num">8 800 200 41 46</a>
        <span class="call-free">Звонок по России бесплатный</span>
      </div>
    </div>
    <div class="col-md-3 col-xs-12">
      <p class="flab hidden-md hidden-lg text-center">© Tiroki,2017</p>
    </div>
    <a href="#" class="top"><img src="image/catalog/left-arrow.png" alt=""></a>
  </div>
</div>
</footer>

</div>
<!--
OpenCart is open source software and you are free to remove the powered by OpenCart if you want, but its generally accepted practise to make a small donation.
Please donate via PayPal to donate@opencart.com
//-->

<!-- Theme created by Welford Media for OpenCart 2.0 www.welfordmedia.co.uk -->
<script src="catalog/view/javascript/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>


<script src="catalog/view/javascript/scripts.min.js"></script>

<script type="text/javascript" src="catalog/view/javascript/pdqo/vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
<script type="text/javascript" src="catalog/view/javascript/pdqo/vendor/numbox/jquery.numbox-1.2.0.min.js"></script>
<script type="text/javascript" src="catalog/view/javascript/pdqo/vendor/masked-input/jquery.mask.min.js"></script>
<script type="text/javascript" src="catalog/view/javascript/pdqo/jquery.pdqo-core.min.js"></script>
<!--<link href="catalog/view/javascript/number.css" rel="stylesheet" type="text/css" />
<script src="catalog/view/javascript/number.js" type="text/javascript"></script>-->
<!-- BEGIN JIVOSITE CODE {literal} -->
<script type='text/javascript'>
(function(){ var widget_id = 'vuejpaanpV';var d=document;var w=window;function l(){
var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);}if(d.readyState=='complete'){l();}else{if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})();</script>
<!-- {/literal} END JIVOSITE CODE -->
<script type="text/javascript">
       /* $(".product-colich input[type=number]").number();*/
        $(document).ready(function() {
            $('.minus').click(function () {
                var $input = $(this).parent().parent().find('input');
                var count = parseInt($input.val()) - 1;
                count = count < 1 ? 1 : count;
                $input.val(count);
                $input.change();
                var $sbm = $(this).parent().parent().find('button');
                $sbm.click();
                return false;
            });
            $('.plus').click(function () {
                var $input = $(this).parent().parent().find('input');
                $input.val(parseInt($input.val()) + 1);
                $input.change();
                var $sbm = $(this).parent().parent().find('button');
                $sbm.click();
                return false;
            });


            jQuery('.top-menu .mobile-icon').on( 'click', function(){

                if(jQuery('.top-menu ul.active-mn').length >=1) {
                    jQuery('.top-menu ul').removeClass("active-mn");
                } else {
                    jQuery('.top-menu ul').addClass("active-mn");
                }
            })
            jQuery('.top-menu ul').on( 'click', function(){
                jQuery('.top-menu ul').removeClass("active-mn");
            })
            jQuery('.search-top .search-mob').on( 'click', function(){

                if(jQuery('.search-top #search.search-active').length >=1) {
                    jQuery('.search-top #search').removeClass("search-active");
                } else {
                    jQuery('.search-top #search').addClass("search-active");
                }
            })
            jQuery('.search-top .search-mob').on( 'click', function(){

                if(jQuery('.search-top.add-st-sr').length >=1) {
                    jQuery('.search-top').removeClass("add-st-sr");
                } else {
                    jQuery('.search-top').addClass("add-st-sr");
                }
            })
        })
    </script>
    <script>
  $(document).ready(function() {
	$('#qSend').on('click', function(e){   
        e.preventDefault();
        var res = $('#qContact').serializeArray();
        var arr = {};
        $.each(res, function (result) {
             var $index = res[result].name;
             arr[$index] = res[result].value;
        });
        $.ajax({
            url: 'index.php?route=common/footer/qContact',
            type: 'post',
            dataType: 'json',
            data: arr,
            success: function () {
            }
        });
        $('#callback-name').val('');
        $('#callback-email').val('');
        $('#callback-text').val('');
        swal("Сообщение отправлено", "", "success");
     });
});
</script>

</body></html>