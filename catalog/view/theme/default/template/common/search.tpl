<form action="" method="post" id="search" class="search-form hidden-sm hidden-xs">
    <input type="text" name="search" value="<?php echo $search; ?>" class="search-input"/>
    <input type="submit" name="" value="&#xf002;" class="search-submit"/>
    <button type="button" value="&#xf002;" class="btn btn-default btn-lg search-submit"
            style="height: 31px;opacity: 0;"></button>
</form>
<a id="search" class="search-popup-btn hidden-md hidden-lg" href="#search-mob-form"><img
            src="image/catalog/search.png"
            class="img-responsive"
            alt="">
</a>
<form id="search-mob-form" class="white-popup-block mfp-hide">
    <input type="text" name="" value="" class="search-input"/>
    <input type="submit" name="" value="&#xf002;" class="search-submit"/>
    <a href="index.php?route=product/search" type="button" value="&#xf002;" class="btn btn-default btn-lg search-submit"
            style="height: 31px;opacity: 0;"></a>
</form>