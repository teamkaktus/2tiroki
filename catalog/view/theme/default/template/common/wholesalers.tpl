<?php echo $header; ?>
<section id="page-title" style="background-image: url(image/catalog/bg-large.jpg);">
  <div class="container">
    <div class="row">
      <h3>cпециализированный магазин</h3>
      <h2>Умных часов для детей</h2>
    </div>
  </div>
</section>
<section id="breadcrumbs">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <Ul>
          <li><a href="">Главная</a></li>
          <li>Оптовикам</li>
        </Ul>
      </div>
    </div>
  </div>
</section>

<div class="content">
  <div class="container">
    <h2 class="h2-page-title">Оптовикам</h2>
    <div class="row">
      <div class="col-md-6">
        <img src="image/catalog/opt-2.jpg" alt="" class="img-responsive">
      </div>
      <div class="col-md-6">
        <div class="page-descr">
          <p>Помимо розничной торговли, осуществляем оптовые поставки умных детских часов ТИРОКИ в салоны связи, магазины, школы. Являясь учредителями фабрики, мы готовы предоставить низкие цены и выгодные условия закупки. Действует гибкая система скидок, предоставляются необходимые документы: накладные, счета-фактуры, чеки, сертификаты. В числе постоянных клиентов такие известные организации, как салоны «Связной», «Евросеть», «Мегафон», Всероссийская служба поиска пропавших детей.</p>
          <p>Мы заинтересованы в успешном бизнесе каждого партнера, поэтому всегда открыты к конструктивному диалогу. Приглашаем к взаимовыгодному сотрудничеству заинтересованные организации по всей стране!</p>
        </div>
      </div>
    </div>
  </div>
</div>
<section class="back-form">
  <div class="container">
    <div class="row">
      <div class="back-call" style="background-image: url(image/catalog/cat-bg.png);">

        <form id="oContact">
          <label for="name">Интересуетесь оптом?</label>
          <input class="opt-input-color" type="text" id="name" name="oName" placeholder="Ваше имя">
          <input class="opt-input-color" type="text" id="phone" name="oPhone" placeholder="Ваш телефон">
          <input id="oSend" type="submit" value="Отправить">
        </form>

      </div>
    </div>
  </div>
</section>
<section class="common-cont">
  <div class="container">
    <div class="row">
      <div class="com-cont">
        <div class="row">
          <div class="col-md-12">
            <h4 class="partners">Мы работаем с ведущими транспортными компаниями</h4>
          </div>
        </div>
        <div class="row partners-row ">
          <div class="col-md-2 col-xs-6 col-md-offset-1  hidden-xs">
            <a href="#"><img src="image/catalog/post.png" class="img-responsive" alt=""></a>
          </div>
          <div class="col-md-2 col-xs-6  hidden-xs">
            <a href="#"><img src="image/catalog/opt-pamek-img.png" class="img-responsive" alt=""></a>
          </div>
          <div class="col-md-2 col-xs-3  hidden-xs">
            <a href="#"><img src="image/catalog/opt-nek-img.png" class="img-responsive" alt=""></a>
          </div>
          <div class="col-md-2 col-xs-3  hidden-xs">
            <a href="#"><img src="image/catalog/cdek.png" class="img-responsive" alt=""></a>
          </div>
          <div class="col-md-2 col-xs-6  hidden-xs">
            <a href="#"><img src="image/catalog/opt-kyt-img.png" class="img-responsive" alt=""></a>
          </div>
          <div class="col-xs-12 visible-xs"><img clas="img-responsive" style="width: 100%;" src="image/catalog/partners-1.png" alt=""></div>
        </div>
      </div>
    </div>
  </div>
</section>
<script type="text/javascript">
$(function(){
    $("#phone").mask("+7 (999) 999-99-99");
});
</script>
<script>
  $(document).ready(function() {
	$('#oSend').on('click', function(e){  
        e.preventDefault();      
        var res = $('#oContact').serializeArray();
        var arr = {};
        $.each(res, function (result) {
             var $index = res[result].name;
             arr[$index] = res[result].value;
        });
        $.ajax({
            url: 'index.php?route=common/wholesalers/opContact',
            type: 'post',
            dataType: 'json',
            data: arr,
            success: function () {
            }
        });
        $('#name').val('');
        $('#phone').val('');
        swal("Сообщение отправлено", "", "success");
     });
  });
</script>
<?php echo $footer; ?>