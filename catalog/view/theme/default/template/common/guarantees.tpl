<?php echo $header; ?>
<section id="page-title" style="background-image: url(image/catalog/bg-large.jpg);">
  <div class="container">
    <div class="row">
      <h3>cпециализированный магазин</h3>
      <h2>Умных часов для детей</h2>
    </div>
  </div>
</section>

<section id="breadcrumbs">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <Ul>
          <li><a href="">Главная</a></li>
          <li>Гарантии</li>
        </Ul>
      </div>
    </div>
  </div>
</section>

<div class="content">
  <div class="container">
    <h2 class="h2-page-title">Гарантии</h2>
    <div class="row">
      <div class="col-md-6">
        <img src="image/catalog/garantii_new.jpg" alt="" class="img-responsive">
      </div>
      <div class="col-md-6">
        <div class="page-descr">
          <p>Мы отвечаем за качество часов ТИРОКИ, однако даже у самых надежных изделий случаются поломки или заводской брак. В случае обнаружения проблем, возникших не по вине покупателя, товар можно вернуть или обменять на аналогичную или другую модель. Обмен или возврат магазином производится в течение 14 дней с момента покупки или получения в почтовом отделении вашего региона.</p>
          <p>Гарантийные обязательства завода-изготовителя действуют в течение 1 года с момента приобретения. На протяжении этого времени вы можете обращаться в наш сервис по ремонту и обслуживанию совершенно бесплатно.</p>
        </div>
      </div>
    </div>
  </div>
</div>

<section class="common-cont">
  <div class="container">
    <h2 class="blue">Гарантии</h2>
    <div class="row">
      <div class="com-cont">
        <div class="row">
          <div class="col-md-3 mar_booott">
            <div class="com-wrap">
              <img src="image/catalog/garant1.png" alt="" class="gar-img">
              <span>Гарантия 1 год</span>
            </div>
          </div>
          <div class="col-md-3 mar_booott">
            <div class="com-wrap">
              <img src="image/catalog/garant2.png" alt="" class="gar-img">
              <span>Возврат товара в течении 14 дней со дня покупки по законам РФ</span>
            </div>
          </div>
          <div class="col-md-3 mar_booott">
            <div class="com-wrap">
              <img src="image/catalog/garant3.png" alt="" class="gar-img">
              <span>Сертификат гост</span>
            </div>
          </div>
          <div class="col-md-3 mar_booott">
            <div class="com-wrap">
              <img src="image/catalog/garant4.png" alt="" class="gar-img">
              <span>При покупке вы получаете все необходимые документы</span>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-3 mar_booott">
            <div class="com-wrap">
              <img src="image/catalog/garant5.png" alt="" class="gar-img">
              <span>Оплата при получении</span>
            </div>
          </div>
          <div class="col-md-3 mar_booott">
            <div class="com-wrap">
              <img src="image/catalog/garant6.png" alt="" class="gar-img">
              <span>Онлайн платежи все надежно защищены сервисом ROBOKASSA</span>
            </div>
          </div>
          <div class="col-md-3 mar_booott">
            <div class="com-wrap">
              <img src="image/catalog/garant7.png" alt="" class="gar-img">
              <span>Все отправления застрахованы</span>
            </div>
          </div>
          <div class="col-md-3 mar_booott">
            <div class="com-wrap">
              <img src="image/catalog/garant8.png" alt="" class="gar-img">
              <span>Мы работаем правильно !</span>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!--<div class="container">
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?><?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>-->
<?php echo $footer; ?>