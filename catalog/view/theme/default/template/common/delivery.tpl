<?php echo $header; ?>
<section id="page-title" style="background-image: url(image/catalog/bg-large.jpg);">
  <div class="container">
    <div class="row">
      <h3>cпециализированный магазин</h3>
      <h2>Умных часов для детей</h2>
    </div>
  </div>
</section>
<section id="breadcrumbs">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <Ul>
          <li><a href="">Главная</a></li>
          <li>Оплата и доставка</li>
        </Ul>
      </div>
    </div>
  </div>
</section>
<div class="content">
  <div class="container">
    <h2 class="h2-page-title">Доставка и оплата</h2>
    <div class="row">
      <div class="col-md-6 mb">
        <img src="image/catalog/partners.jpg" alt="" class="img-responsive">
      </div>
      <div class="col-md-6">
        <div class="page-descr">
          <p>Наша компания предлагает различные способы доставки и оплаты товара, удобные потребителям. Привозим сформированный заказ клиентам, проживающим в Москве, на следующий день после подачи заявки. Стоимость услуг курьера в пределах МКАД, по Московской области, уточняйте у менеджеров нашей компаниии.</p>
          <p>Для заказчиков, проживающих в регионах, организуем наложенную пересылку умных детских часов ТИРОКИ нашим партнером – Почтой России, стоимость отправки уточняйте у менеджеров нашей компаниии. Оплата при получении. При желании расчет можно осуществить на нашем сайте при помощи электронных кошельков Qiwi, Яндекс-Деньги, а также любой банковской картой систем Visa, Master Card.</p>
        </div>
      </div>
    </div>
  </div>
</div>

<section class="common-cont">
  <div class="container">
    <h2 class="blue">Доставка</h2>
    <div class="row">
      <div class="com-cont">
        <div class="row">
          <div class="col-md-4">
            <div class="com-wrap com-wrap___style_text">
              <img src="image/catalog/com-1.png" alt="">
              <span>Доставка собственной курьерской службой по Москве и Московской области 1-2 дня </span>
            </div>
          </div>
          <div class="col-md-4">
            <div class="com-wrap com-wrap___style_text">
              <img src="image/catalog/com-2.png" alt="">
              <span>Курьеры пунктуальные и компетентные  сотрудники , при желании продемонстрируют и проведут настройку гаджета</span>
            </div>
          </div>
          <div class="col-md-4">
            <div class="com-wrap com-wrap___style_text">
              <img src="image/catalog/com-3.png" alt="">
              <span>Доставка по России осуществляется ведущими транспортными компаниями</span>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-4">
            <div class="com-wrap com-wrap___style_text">
              <img src="image/catalog/deliv7.png" alt="">
              <span>Более 800 пунктов выдачи заказов по всей России</span>
            </div>
          </div>
          <div class="col-md-4">
            <div class="com-wrap com-wrap___style_text">
              <img src="image/catalog/com-5.png" alt="">
              <span>Возможность отследить направление на сайте почты России.</span>
            </div>
          </div>
          <div class="col-md-4">
            <div class="com-wrap com-wrap___style_text">
              <img src="image/catalog/com-6.png" alt="">
              <span>Все отправления застрахованы</span>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="common-cont">
  <div class="container">
    <h2 class="red">Оплата</h2>
    <div class="row">
      <div class="com-cont">
        <div class="row">
          <div class="col-md-4">
            <div class="com-wrap">
              <img src="image/catalog/com-7.png" alt="">
              <span>Онлайн оплата у нас на сайте</span>
            </div>
          </div>
          <div class="col-md-4">
            <div class="com-wrap">
              <img src="image/catalog/com-8.png" alt="">
              <span>Оплата при получении курьеру</span>
            </div>
          </div>
          <div class="col-md-4">
            <div class="com-wrap">
              <img src="image/catalog/com-9.png" alt="">
              <span>Орлата при получении в пункте выдачи заказа</span>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php echo $footer; ?>