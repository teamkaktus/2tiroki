<?php echo $header; ?>
<section id="page-title" style="background-image: url(image/catalog/bg-large.jpg);">
  <div class="container">
    <div class="row">
      <h3>cпециализированный магазин</h3>
      <h2>Умных часов для детей</h2>
    </div>
  </div>
</section>
<section id="breadcrumbs">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <Ul>
          <li><a href="">Главная</a></li>
          <li>Что такое умные часы TIROKI</li>
        </Ul>
      </div>
    </div>
  </div>
</section>
<div class="content">
  <div class="container">
    <h2 class="h2-page-title">Что такое умные часы TIROKI</h2>
    <div class="row">
      <div class="col-md-12">
        <img src="image/catalog/pic-2.jpg" alt="" style="width: 100%;" class="img-responsive">
      </div>
    </div>
  </div>
</div>
<section class="smart-clocks">
  <div class="container">
    <div class="row">
      <div class="col-md-6 col-md-push-6">
        <img src="image/catalog/girl.png" class="img-responsive" alt="">
      </div>
      <div class="col-md-6 col-md-pull-6">
        <h4>Как использовать умные часы Tiroki</h4>
        <div class="smart-clocks-item">
          <img src="image/catalog/smart.png" alt="">
          <p>Покупаете часы в нашем магазине, вставляете сим-карту в гаджет малыша. </p>
        </div>
        <div class="smart-clocks-item">
          <img src="image/catalog/smart.png" alt="">
          <p>Скачиваете мобильное приложение из Play Market или App Store.</p>
        </div>
        <div class="smart-clocks-item">
          <img src="image/catalog/smart.png" alt="">
          <p>Синхронизируете сотовый номер ребенка с данными приложения. </p>
        </div>
        <div class="smart-clocks-item">
          <img src="image/catalog/smart.png" alt="">
          <p>Вносите нужные контакты в телефонную книгу ТИРОКИ. </p>
        </div>
        <div class="smart-clocks-item">
          <img src="image/catalog/smart.png" alt="">
          <p>Теперь ребенок всегда будет в зоне внимания и контроля </p>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="functions">
  <div class="container">
    <h2>Функции наших часов:</h2>
    <div class="row">
      <div class="col-md-4 col-md-push-4">
        <img src="image/catalog/big-clocks.png" class="img-responsive center-block big-clocks" alt="">
      </div>
      <div class="col-md-4 col-md-pull-4">
        <div class="functions-item func_items">
          <img src="image/catalog/func-1.png" class="img-responsive" alt="">
          <div class="func-descr">
            <h3 class="red">Геолокация</h3>
            <p>Вы видите на карте местоположение ребенка в реальном времени</p>
          </div>
        </div>
        <div class="functions-item func_items">
          <img src="image/catalog/funkc2.png" class="img-responsive" alt="">
          <div class="func-descr">
            <h3 class="red">Аудио контроль</h3>
            <p>Вы слышите, что происходит вокруг ребенка</p>
          </div>
        </div>
        <div class="functions-item func_items">
          <img src="image/catalog/funkc3.png" class="img-responsive" alt="">
          <div class="func-descr">
            <h3 class="red">GSM связь</h3>
            <p>Вы можете разговаривать с ребенком как по обычному сотовому</p>
          </div>
        </div>
        <div class="functions-item func_items">
          <img src="image/catalog/funkc4.png" class="img-responsive" alt="">
          <div class="func-descr">
            <h3 class="red">Голосовые SMS</h3>
            <p>Вы можете посылать друг другу голосовые сообщения</p>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="functions-item func_items">
          <img src="image/catalog/funkc5.png" class="img-responsive" alt="">
          <div class="func-descr">
            <h3 class="red">Кнопка SOS</h3>
            <p>При опасности ребенок нажимает на кнопку и вы на связи</p>
          </div>
        </div>
        <div class="functions-item func_items">
          <img src="image/catalog/funkc6.png" class="img-responsive" alt="">
          <div class="func-descr">
            <h3 class="red">Геозабор</h3>
            <p>Установите ограничение и часы сообщат об отдалении ребенка</p>
          </div>
        </div>
        <div class="functions-item func_items">
          <img src="image/catalog/funkc7.png" class="img-responsive" alt="">
          <div class="func-descr">
            <h3 class="red">Награды</h3>
            <p>За хорошие поступки вы поощряете ребенка, посылая ему сердечки</p>
          </div>
        </div>
        <div class="functions-item func_items">
          <img src="image/catalog/funkc8.png" class="img-responsive" alt="">
          <div class="func-descr">
            <h3 class="red">Датчик снятия</h3>
            <p>Если ребенок снимет часы вы сразу узнаете об этом и примите меры</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="interesting">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="int-wrap">
          <p><span class="blue">Умные часы Тироки</span> – удивительный симбиоз необходимого наручного аксессуара и обязательного средства связи между ребенком, родителями и близкими людьми. Обладая внешним видом хронометра, устройство показывает точное время, так что мальчик или девочка никогда не опоздает в школу. Громкий будильник напомнит о себе в установленные часы, а секундомер поможет засечь время на дорогу от дома до школы. Вместе с тем девайс обладает всеми функциями мобильного телефона: вставленная микро-сим карта позволит всегда оставаться на связи с родными и близкими людьми.</p>
          <p>Однако главная прелесть девайса заключается в другом. С помощью часов ТИРОКИ вы больше не будет волноваться за ребенка, недоумевая, куда пропал малыш после школьных занятий. Надо только установить приложение в свой смартфон или планшет, синхронизировать его с номером сим-карты ребенка, и возможность отслеживать место пребывания малыша активирована! Встроенный GPS-трекер точно определяет положение ребенка, при выходе за пределы установленной зоны родители получат уведомление. История передвижений за день сохраняется, благодаря чему вечером можно узнать, где побывал ребенок, что видел, с кем общался.</p>
          <p>Не забыты и интересы детей: часы обладают полноценными функциями мобильного телефона. Ребенок может звонить по любому номеру из книги контактов, отправлять голосовые и текстовые сообщения родителям и друзьям с помощью мессенджеров. Некоторые модели поддерживают беспроводную связь Wi-Fi, благодаря чему общение становится поистине безграничным. Подростки могут пользоваться мобильным интернетом для учебы и развлечений: читать книги, смотреть фильмы, общаться с одноклассниками. Яркий дизайн станет предметом пристального внимания окружающих.</p>
          <p>Вопрос безопасности ребенка – главная проблема всех родителей. Решать ее приходится разными способами, но благодаря умным часам ТИРОКИ волнений станет существенно меньше. Знать, где находится ребенок, слышать, о чем говорят окружающие в радиусе 5 метров (гаджет предусматривает удаленный аудио-мониторинг), не беспокоиться за самочувствие ребенка – не об этом ли мечтают все родители? Иметь современное средство коммуникации со стильным дизайном и мощными возможностями – разве не этого хочет любой ребенок? Умные часы ТИРОКИ – идеальная вещь, которая понравится взрослым и детям!</p>
        </div>
      </div>
    </div>
  </div>
</section>
<!--<div class="container">
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?><?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
-->
<?php echo $footer; ?>