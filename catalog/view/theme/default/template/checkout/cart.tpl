<?php echo $header; ?>
<section id="page-title" style="background-image: url(image/catalog/bg-large.jpg);">
  <div class="container">
    <div class="row">
      <h3>cпециализированный магазин</h3>
      <h2>Умных часов для детей</h2>
    </div>
  </div>
</section>
  <section id="breadcrumbs">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <Ul>
            <li><a href="">Главная</a></li>
            <li>Корзина</li>
          </Ul>
        </div>
      </div>
    </div>
  </section>
<section class="zakaz">
<div class="container">
  <?php if ($attention) { ?>
  <div class="alert alert-info"><i class="fa fa-info-circle"></i> <?php echo $attention; ?>
    <button type="button" class="close" data-dismiss="alert">&times;</button>
  </div>
  <?php } ?>
  <?php if ($success) { ?>
  <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
    <button type="button" class="close" data-dismiss="alert">&times;</button>
  </div>
  <?php } ?>
  <?php if ($error_warning) { ?>
  <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
    <button type="button" class="close" data-dismiss="alert">&times;</button>
  </div>
  <?php } ?>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="col-sm-12 zakaz"><?php echo $content_top; ?>
      <h1 class="h2-page-title">Корзина</h1>
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
        <div class="">
            <div class="zakaz-zakaz">
              <?php foreach ($products as $product) { ?>
              <div class="row">
                <div class="zakaz-wrapp">
                  <?php if ($product['thumb']) { ?>
                    <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" /></a>
                  <?php } ?>
                    <a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
                  <?php if (!$product['stock']) { ?>
                  <span class="text-danger">***</span>
                  <?php } ?>
                  <?php if ($product['option']) { ?>
                  <?php foreach ($product['option'] as $option) { ?>
                  <br />
                  <small><?php echo $option['name']; ?>: <?php echo $option['value']; ?></small>
                  <?php } ?>
                  <?php } ?>
                  <?php if ($product['reward']) { ?>
                  <br />
                  <small><?php echo $product['reward']; ?></small>
                  <?php } ?>
                  <?php if ($product['recurring']) { ?>
                  <br />
                  <span class="label label-info"><?php echo $text_recurring_item; ?></span> <small><?php echo $product['recurring']; ?></small>
                  <?php } ?>
                    <div class="form-btn">
                    <label for="qual">Количество: </label>
                      <button style="display:none;" type="submit" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Обновить"><i class="fa fa-refresh"></i></button>
                      <input type="number" min="1" max="99999" step="1" pattern="[0-9]*" name="quantity[<?php echo $product['cart_id']; ?>]" value="<?php echo $product['quantity']; ?>" size="2" id="input-quantity"/>
                      <div class="btns">
                        <span class="minus">-</span>
                        <span class="plus">+</span>
                      </div>
                    </div>
                    <div class="price">
                      <span>Цена:</span> <?php echo $product['price']; ?>
                    </div>
                    <div class="total">
                      <span>Сумма:</span> <?php echo $product['total']; ?>
                    </div>
                
					<button type="button" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="delete btn btn-danger" onclick="cart.remove('<?php echo $product['cart_id']; ?>');"><i class="fa fa-times-circle"></i></button>
                </div>
              </div>
                <div style="clear:both"></div>
              <?php } ?>
			</div>
		  
		</div>
      </form>


      <div class="row">
        <div class="col-sm-4 col-sm-offset-8">
            <?php $i=0; foreach ($totals as $total) { $i++ ?>
				<div class="text-right cart-product-total total-price" style="margin-top:0;padding-left:0 !important;"><span>Итого:</span> <?php echo $total['text']; ?></div>
            <?php if($i=1) break; } ?>
        </div>
      </div>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
</section>

<section class="additional">
<div class="container">        
    
    <div class="error"></div>
        
    <?php if (!isset($address)) { ?>
    <div class="row">
      <div class="col-md-12">
          <h2 class="checkout-title">Оформление заказа</h2>
          
          <div class="login-form registerbox clearfix" style="display:none">
        <div class="row">
        <div class="col-md-12 message"></div>
        <form class="form-inline" role="form">
              <div class="col-md-8">
        <div class="form-group">
          <label class="control-label" for="input-email"><?php// echo $entry_email; ?></label>
          <input type="text" name="email" value="" placeholder="<?php echo str_replace(':','',$entry_email); ?>" id="input-email" class="form-control" />
        </div>&nbsp;&nbsp;
        <div class="form-group">
          <label class="control-label" for="input-password"><?php echo $entry_password; ?></label>
          <input type="password" name="password" value="" placeholder="<?php echo str_replace(':','',$entry_password); ?>" id="input-password" class="form-control" />
        </div>
        </div>
        <div class="form-group col-md-4">
            <input type="button" value="<?php echo $button_login; ?>" id="button-login" data-loading-text="<?php if (isset($text_loading)) echo $text_loading;else echo 'loading ...' ?>" class="btn btn-primary" />
            <a href="<?php echo $forgotten; ?>"><?php echo $text_forgotten; ?></a>
        </div>
            </form>
          </div>
      </div>
        </div>
    </div>
    <?php }?>
    
    <div class="row box checkout_form">
        <div class="col-md-6 register_block">
      <div class="row">
          <div class="col-md-12">
            <h3 class="person-dan">Персональные данные</h3>
          </div>

        <div class=" required col-md-6 style-inp">
          <input type="text" name="lastname" value="<?php if (isset($lastname)) echo $lastname;?>" placeholder="Фамилия*" id="input-payment-lastname" class="cart-inpute-name" <?php if (isset($customer_id)) {?> readonly<?php }?>/>
        </div>

          <div class=" required col-md-6 style-inp">
            <input type="text" name="firstname" value="<?php if (isset($address['firstname'])) echo $address['firstname']; elseif (isset($firstname)) echo $firstname; ?>" placeholder="Имя*" id="input-payment-firstname" class="cart-inpute-name" <?php if (isset($customer_id)) {?> readonly<?php }?>/>
          </div>

        <div class=" required col-md-6 style-inp">
			<input type="text" name="fax" value="<?php if (isset($fax)) echo $fax;?>" placeholder="Отчество" id="input-payment-fax" class="cart-inpute-name" <?php if (isset($customer_id)) { ?> readonly<?php }?>/>
        </div>
        <div class=" required col-md-6 style-inp">
          <input type="text" name="email" value="<?php if (isset($email)) echo $email;?>" placeholder="Email*" id="input-payment-email" class="cart-inpute-name" <?php if (isset($customer_id)) {?> readonly<?php }?>/>
        </div>
          <div class="form-group required  col-md-12 style-inp">
            <input type="text" name="telephone" value="<?php if (isset($telephone)) echo $telephone;?>" placeholder="Ваш телефон*" id="input-payment-telephone" class="cart-inpute-name" <?php if (isset($customer_id)) {?> readonly<?php }?>/>
          </div>

        <div class="shipping-method check-dost shipping-text-style hidden-xs">
          <?php if ($error_warning) { ?>
          <div class="alert alert-warning"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
          <?php } ?>
          <?php if ($shipping_methods) { ?>
          <p><?php echo $text_shipping_method; ?></p>
          <?php foreach ($shipping_methods as $shipping_method) { ?>
          <p><strong><?php echo $shipping_method['title']; ?></strong></p>
          <?php if (!$shipping_method['error']) { ?>
          <?php foreach ($shipping_method['quote'] as $quote) { ?>
          <div class="radio">
            <label>
              <?php if ($quote['code'] == $code || !$code) { ?>
              <?php $code = $quote['code']; ?>
              <input type="radio" name="shipping_method" value="<?php echo $quote['code']; ?>"  title="<?php echo $quote['title']; ?>" checked="checked" />
              <?php } else { ?>
              <input type="radio" name="shipping_method" value="<?php echo $quote['code']; ?>"  title="<?php echo $quote['title']; ?>" />
              <?php } ?>
              <?php echo $quote['title']; ?> - <?php echo $quote['text']; ?></label>
          </div>
          <?php } ?>
          <?php } else { ?>
          <div class="alert alert-danger"><?php echo $shipping_method['error']; ?></div>
          <?php } ?>
          <?php } ?>
          <?php } ?>
        </div>
        <div class="register-form" style="display:none">
              <div class="form-group required col-md-6">
          <label class="control-label" for="input-payment-password"><?php echo $entry_password; ?></label>
          <input type="password" name="password" value="" placeholder="<?php echo str_replace(':','',$entry_password); ?>" id="input-payment-password" class="form-control" />
              </div>
              <div class="form-group required col-md-6">
          <label class="control-label" for="input-payment-confirm"><?php echo $entry_confirm; ?></label>
          <input type="password" name="confirm" value="" placeholder="<?php echo str_replace(':','',$entry_confirm); ?>" id="input-payment-confirm" class="form-control" />
              </div>
        </div>
          </div>
        </div>            
        <div class="col-md-6">


        <div class="col-md-12">
          <h3 class="clearfix person-dan">Адрес доставки</h3>

          
          <?php if (isset($customer_id)) { ?>
          <div class="radio">
            <label>
              <input type="radio" name="payment_address" value="existing" checked="checked" onclick="jQuery('#payment-address-new').hide()" />
              <?php echo $text_address_existing; ?></label>
          </div>
          <div id="payment-existing">
            <select name="payment_address_id" class="form-control">
              <?php foreach ($addresses as $address) { ?>
              <?php if (isset($payment_address_id) && $address['address_id'] == $payment_address_id) { ?>
              <option value="<?php echo $address['address_id']; ?>" selected="selected"><?php echo $address['firstname']; ?> <?php echo $address['lastname']; ?>, <?php echo $address['address_1']; ?>, <?php echo $address['city']; ?>, <?php echo $address['zone']; ?>, <?php echo $address['country']; ?></option>
              <?php } else { ?>
              <option value="<?php echo $address['address_id']; ?>"><?php echo $address['firstname']; ?> <?php echo $address['lastname']; ?>, <?php echo $address['address_1']; ?>, <?php echo $address['city']; ?>, <?php echo $address['zone']; ?>, <?php echo $address['country']; ?></option>
            
              <?php } ?>
            </select>
          </div>
          <?php } ?>
          <?php if (isset($customer_id)) { ?>
          <div class="radio">
            <label>
              <input type="radio" name="payment_address" value="new" onclick="jQuery('#payment-address-new').show();"/>
              <?php echo $text_address_new; ?></label>
          </div>
          <?php } ?>
          <?php } ?>
        </div>

        <div id="payment-address-new" <?php if (isset($customer_id) && $addresses) { ?> style="display:none"<?php }?>>


        <div class="form-group required col-md-12 style-inp" style="display: none;">
          <input type="text" name="city" value="---" placeholder="Город" id="input-payment-city" class="form-control" />
        </div>

             <div class="form-group required col-md-12 style-inp" style="padding-left: 0">
          <textarea style="font-family: inherit;" name="address_1" placeholder="Адрес*" id="input-payment-address-1" rows="8" class="cart-inpute-textarea" /></textarea>
        </div>
        <div class="form-group required col-md-6 style-inp" style="display: none;">
          <input type="text" name="postcode" value="<?php if (isset($postcode)) echo $postcode;?>" placeholder="Дом" id="input-payment-postcode" class="form-control" />
        </div>
        <input type="hidden" name="company" value="" />

        <?php if (isset($entry_company_id)) { ?>
        <?php if (!$checkout_hide_company_id){ ?>
        <?php } else { ?>
        <input type="hidden" name="company_id" value="" />
        <?php }?>

                <div class="form-group required col-md-6" style="display: none;">
          <label class="control-label" for="input-payment-postcode"><?php echo $entry_postcode; ?></label>
          <input type="text" name="postcode" value="<?php if (isset($postcode)) echo $postcode;?>" placeholder="" id="input-payment-postcode" class="form-control" />
        </div>

        <?php if (!$checkout_hide_tax_id){ ?>
        <div class="form-group col-md-6 style-inp" style="display: none;">
          <label class="control-label" for="input-tax-id-"><?php echo $entry_tax_id; ?></label>
          <input type="text" name="tax_id" value="<?php if (isset($tax_id)) echo $tax_id; ?>" placeholder="<?php echo str_replace(':','',$entry_tax_id); ?>" id="input-tax-id-" class="form-control" />
        </div>
        <?php } else { ?>
        <input type="hidden" name="tax_id" value="" />
        <?php } }?>

        <div class="form-group col-md-6 style-inp" style="display: none;">
          <input type="text" name="address_2" value="<?php if (isset($address_2)) echo $address_2;?>" placeholder="Квартира" id="input-payment-address-2" class="form-control" />
        </div>

        <div class="form-group required col-md-6" style="display: none;">
          <label class="control-label" for="input-payment-country"><?php echo $entry_country; ?></label>
          <select name="country_id" id="input-payment-country" class="form-control">
            <option value="176">Россия</option>
          </select>
        </div>
        <div class="form-group required col-md-6" style="display: none;">
          <label class="control-label" for="input-payment-zone"><?php echo $entry_zone;; ?></label>
          <select name="zone_id" id="input-payment-zone" class="form-control">
            <option value="2761">Москва</option>
          </select>
        </div>
      </div>
          
          <div class="shipping-method check-dost shipping-text-style visible-xs">
          <?php if ($error_warning) { ?>
          <div class="alert alert-warning"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
          <?php } ?>
          <?php if ($shipping_methods) { ?>
          <p><?php echo $text_shipping_method; ?></p>
          <?php foreach ($shipping_methods as $shipping_method) { ?>
          <p><strong><?php echo $shipping_method['title']; ?></strong></p>
          <?php if (!$shipping_method['error']) { ?>
          <?php foreach ($shipping_method['quote'] as $quote) { ?>
          <div class="radio">
            <label>
              <?php if ($quote['code'] == $code || !$code) { ?>
              <?php $code = $quote['code']; ?>
              <input type="radio" name="shipping_method" value="<?php echo $quote['code']; ?>"  title="<?php echo $quote['title']; ?>" checked="checked" />
              <?php } else { ?>
              <input type="radio" name="shipping_method" value="<?php echo $quote['code']; ?>"  title="<?php echo $quote['title']; ?>" />
              <?php } ?>
              <?php echo $quote['title']; ?> - <?php echo $quote['text']; ?></label>
          </div>
          <?php } ?>
          <?php } else { ?>
          <div class="alert alert-danger"><?php echo $shipping_method['error']; ?></div>
          <?php } ?>
          <?php } ?>
          <?php } ?>
        </div>
      
          <div class="payment-method check-paym">
          <?php if ($error_warning) { ?>
          <div class="alert alert-warning"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
          <?php } ?>
          <?php if ($payment_methods) { ?>
          <p><?php echo $text_payment_method; ?></p>
          <?php foreach ($payment_methods as $payment_method) { ?>
          <div class="radio">
            <label>
              <?php if ($payment_method['code'] == $code || !$code) { ?>
              <?php $code = $payment_method['code']; ?>
              <input type="radio" name="payment_method" value="<?php echo $payment_method['code']; ?>" title="<?php echo $payment_method['title']; ?>" checked="checked" />
              <?php } else { ?>
              <input type="radio" name="payment_method" value="<?php echo $payment_method['code']; ?>" title="<?php echo $payment_method['title']; ?>" />
              <?php } ?>
              <?php echo $payment_method['title']; ?></label>
          </div>
          <?php } ?>
          <?php } ?>
          </div>
          <div style="clear:both;"></div>
		  <!--<?php foreach ($totals as $total) { ?>
                <?php echo $total['title']; ?>: <?php echo $total['text']; ?>
          <?php } ?>-->
          <div class="payment clearfix">
            <div class="pull-right">
              <input type="button" class="btn btn-primary check-btn zakaz-btn button"  id="button-register" value="Подтвердить заказ">
              </div></div>
          </div>
          </div>
      </div>
</section>
    </div>
  </div>    
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>

<script type="text/javascript">
$(function(){
    $("#input-payment-telephone").mask("+7 (999) 999-99-99");
});
</script>

<script type="text/javascript"><!--
var error = true;

// Login
$(document).delegate('#button-login', 'click', function() {
    $.ajax({
        url: 'index.php?route=checkout/checkout/login_validate',
        type: 'post',
        data: $('.login-form :input'),
        dataType: 'json',
        beforeSend: function() {
          $('#button-login').button('loading');
    },  
        complete: function() {
            $('#button-login').button('reset');
        },              
        success: function(json) {
            $('.alert, .text-danger').remove();
            
            if (json['redirect']) {
                location = json['redirect'];
            } else if (json['error']) {
                $('.login-form .message').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
           }
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    }); 
});

// Register
$(document).delegate('#button-register', 'click', function() 
{
  
  var data = $('.checkout_form input[type=\'text\'], .checkout_form input[type=\'date\'], .checkout_form input[type=\'datetime-local\'], .checkout_form input[type=\'time\'], .checkout_form input[type=\'password\'], .checkout_form input[type=\'hidden\'], .checkout_form input[type=\'checkbox\']:checked, .checkout_form input[type=\'radio\']:checked, .checkout_form textarea, .checkout_form select').serialize();
  data += '&_shipping_method='+ jQuery('.checkout_form input[name=\'shipping_method\']:checked').prop('title') + '&_payment_method=' + jQuery('.checkout_form input[name=\'payment_method\']:checked').prop('title');
  
    $.ajax({
        url: 'index.php?route=checkout/checkout/validate',
        type: 'post',
        data: data,
        dataType: 'json',
        
        success: function(json) {
            $('.alert, .text-danger').remove();
                        
            if (json['redirect']) {
                location = json['redirect'];                
            } else if (json['error']) {
        error = true;
                if (json['error']['warning']) {
                    $('.error').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                }
                
    for (i in json['error']) {
                    $('[name="' + i + '"]').after('<div class="text-danger">' + json['error'][i] + '</div>');
    }
            } else 
      {
      error = false;
      jQuery('[name=\'payment_method\']:checked').click();
            }    
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    }); 
});



$('select[name=\'shipping_country_id\']').on('change', function() {
  $.ajax({
        url: 'index.php?route=checkout/checkout/country&country_id=' + this.value,
        dataType: 'json',
        beforeSend: function() {
      $('select[name=\'country_id\']').after(' <i class="fa fa-spinner fa-spin"></i>');
        },
        complete: function() {
      $('.fa-spinner').remove();
        },          
        success: function(json) {
      if (json['postcode_required'] == '1') {
        $('input[name=\'postcode\']').parent().parent().addClass('required');
      } else {
        $('input[name=\'postcode\']').parent().parent().removeClass('required');
      }
                  
            html = '<option value=""><?php echo $text_select; ?></option>';
            
            if (json['zone'] && json['zone'] != '') {
                for (i = 0; i < json['zone'].length; i++) {
                    html += '<option value="' + json['zone'][i]['zone_id'] + '"';
                    
                    if (json['zone'][i]['zone_id'] == '<?php echo $zone_id; ?>') {
                        html += ' selected="selected"';
                    }
    
                    html += '>' + json['zone'][i]['name'] + '</option>';
                }
            } else {
                html += '<option value="0" selected="selected"><?php echo $text_none; ?></option>';
            }
            
            $('select[name=\'shipping_zone_id\']').html(html).val("");
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
});





$('select[name=\'country_id\'], select[name=\'zone_id\'], select[name=\'shipping_country_id\'], select[name=\'shipping_zone_id\'], input[type=\'radio\'][name=\'payment_address\'], input[type=\'radio\'][name=\'shipping_address\']').on('change', function() 
{
  if (this.name == 'contry_id') jQuery("select[name=\'zone_id\']").val("");
  if (this.name == 'shipping_country_id') jQuery("select[name=\'shipping_zone_id\']").val("");
  
    jQuery(".shipping-method").load('index.php?route=checkout/checkout/shipping_method', $('.checkout_form input[type=\'text\'], .checkout_form input[type=\'date\'], .checkout_form input[type=\'datetime-local\'], .checkout_form input[type=\'time\'], .checkout_form input[type=\'password\'], .checkout_form input[type=\'hidden\'], .checkout_form input[type=\'checkbox\']:checked, .checkout_form input[type=\'radio\']:checked,input[name=\'shipping_method\']:first, .checkout_form textarea, .checkout_form select'), function() 
    {
    if (jQuery("input[name=\'shipping_method\']:first").length) 
    {
      jQuery("input[name=\'shipping_method\']:first").attr('checked', 'checked').prop('checked', true).click();
    } else
    {
      jQuery("#cart_table").load('index.php?route=checkout/checkout/cart', $('.checkout_form input[type=\'text\'], .checkout_form input[type=\'date\'], .checkout_form input[type=\'datetime-local\'], .checkout_form input[type=\'time\'], .checkout_form input[type=\'password\'], .checkout_form input[type=\'hidden\'], .checkout_form input[type=\'checkbox\']:checked, .checkout_form input[type=\'radio\']:checked, .checkout_form textarea, .checkout_form select')); 
    }
    });

  jQuery(".payment-method").load('index.php?route=checkout/checkout/payment_method', $('.checkout_form input[type=\'text\'], .checkout_form input[type=\'date\'], .checkout_form input[type=\'datetime-local\'], .checkout_form input[type=\'time\'], .checkout_form input[type=\'password\'], .checkout_form input[type=\'hidden\'], .checkout_form input[type=\'checkbox\']:checked, .checkout_form input[type=\'radio\']:checked,input[name=\'shipping_method\']:first, .checkout_form textarea, .checkout_form select'), function() 
  {
    jQuery("[name=\'payment_method\']").removeAttr("checked").prop('checked', false);
  });
});    


$(document).delegate('input[name=\'shipping_method\']', 'click', function() 
{
    jQuery("#cart_table").load('index.php?route=checkout/checkout/cart', $('.checkout_form input[type=\'text\'], .checkout_form input[type=\'date\'], .checkout_form input[type=\'datetime-local\'], .checkout_form input[type=\'time\'], .checkout_form input[type=\'password\'], .checkout_form input[type=\'hidden\'], .checkout_form input[type=\'checkbox\']:checked, .checkout_form input[type=\'radio\']:checked, .checkout_form textarea, .checkout_form select'));
});  

$('body').delegate('[name=\'payment_method\']','click', function() 
{

  var data = $('.checkout_form input[type=\'text\'], .checkout_form input[type=\'date\'], .checkout_form input[type=\'datetime-local\'], .checkout_form input[type=\'time\'], .checkout_form input[type=\'password\'], .checkout_form input[type=\'hidden\'], .checkout_form input[type=\'checkbox\']:checked, .checkout_form input[type=\'radio\']:checked, .checkout_form textarea, .checkout_form select').serialize();
  data += '&_shipping_method='+ jQuery('.checkout_form input[name=\'shipping_method\']:checked').prop('title') + '&_payment_method=' + jQuery('.checkout_form input[name=\'payment_method\']:checked').prop('title');

  if (!error)
    $.ajax({
        url: 'index.php?route=checkout/checkout/confirm',
        type: 'post',
        data: data,
        success: function(html) 
        {
      jQuery(".payment").html(html);
      
      jQuery("#button-confirm").click();
      
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    }); 
});

$('select[name=\'country_id\']').trigger('change');
jQuery(document).ready(function()
{
  jQuery('input:radio[name=\'payment_method\']:first').attr('checked', true).prop('checked', true);
  <?php /*if ($opencart_version < 2000) { ?>
  $('.colorbox').colorbox({
    width: 640,
    height: 480
  });
  <?php }*/ ?>
});
//--></script> 




<?php echo $footer; ?>
