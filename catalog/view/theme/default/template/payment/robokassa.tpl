<form action="<?php echo $merchant_url; ?>" method="post">
  <div class="buttons">
    <div class="pull-right">
      <input type="submit" value="<?php echo $button_confirm; ?>" class="btn btn-primary" id="rk_btn" />
    </div>
  </div>
</form>
<script>
	setTimeout(function(){
	  $('#rk_btn').trigger('click');
	}, 1000);
</script>