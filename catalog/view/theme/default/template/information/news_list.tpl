<?php echo $header; ?>
<section id="page-title" style="background-image: url(/image/catalog/bg-large.jpg);">
    <div class="container">
        <div class="row">
            <h3>интеренет магазин</h3>
            <h2>детских часов tiroki</h2>
        </div>
    </div>
</section>
<section id="breadcrumbs">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <Ul>
                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                    <?php if($breadcrumb == end($breadcrumbs)) { ?>
                    <li><?php echo $breadcrumb['text']; ?></li>
                    <?php } else { ?>
                    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                    <?php } ?>
                    <?php } ?>
                </Ul>
            </div>
        </div>
    </div>
</section>
<section class="news-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <?php if ($news_list) { ?>
                    <?php foreach ($news_list as $news_item) { ?>
                <div class="news imges2">
                    <?php if($news_item['thumb']) { ?>
                    <img src="<?php echo $news_item['thumb']; ?>" class="img-responsive"/>
                    <?php }?>
                    <div class="news-text">
                        <h4 class="news-title"><?php echo $news_item['title']; ?></h4>
                        <p class="short"><?php echo $news_item['description']; ?></p>
                        <a onclick="location.href = ('<?php echo $news_item['href']; ?>');" class="news-link">Читать дальше</a>
                    </div>
                    </div>
                    <?php } ?>
                <?php } ?>
            </div>
        </div>
    </div>
</section>
<?php echo $footer; ?>