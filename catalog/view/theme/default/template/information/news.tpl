<?php echo $header; ?>
<section id="breadcrumbs">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <Ul>
                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                    <?php if($breadcrumb == end($breadcrumbs)) { ?>
                    <li><?php echo $breadcrumb['text']; ?></li>
                    <?php } else { ?>
                    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                    <?php } ?>
                    <?php } ?>
                </Ul>
            </div>
        </div>
    </div>
</section>
<section class="news-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="news news-cur">
                    <?php if ($thumb) { ?>
                    <img src="<?php echo $thumb; ?>" alt="" class="img-responsive"/>
                    <?php } ?>
                    <div class="news-text">
                        <h4 class="news-title"><?php echo $heading_title; ?></h4>
                        <p class="short"><?php echo $description; ?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php echo $footer; ?>