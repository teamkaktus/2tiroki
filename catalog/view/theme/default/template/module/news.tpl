
	<div class="news hidden-sm hidden-xs">
		<h3 class="cat-title">
			Новости
		</h3>
		<ul>
			<?php foreach ($news as $news_item) { ?>
			<div class="product-layout">
				<div class="product-thumb transition">
					<?php if($news_item['thumb']) { ?>
					<li>
						<h4 class="news-title"><?php echo $news_item['title']; ?></h4>
						<div class="img-thumb-wrap">
							<img src="<?php echo $news_item['thumb']; ?>" alt="" class="news-thumb img-responsive">
						</div>
						<p class="news-text"><?php echo $news_item['description']; ?></p>
						<a onclick="location.href = ('<?php echo $news_item['href']; ?>');" class="news-read-more">Читать дальше</a>
					</li>
					<?php } ?>
				</div>
			</div>
			<?php } ?>
		</ul>
		<a href="index.php?route=information/news" class="button">Все новости</a>
	</div>
