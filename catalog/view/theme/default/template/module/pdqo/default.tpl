<div class="pdqo-window" style="max-width: <?=$window['width'];?>px;">
    <div class="pdqo-wrapper opt_forma22">
        <div class="form-1-pol-133">
            <?=$t['quick_order'];?>
            <a src="catalog/view/image/pdqo/cross.png" class="mfp-close" style="color: white;z-index: 100;cursor: pointer;font-size: 22px;font-family: rotondacBold, sans-serif;text-decoration: none!important;">x</a>
        </div>
        <div class="pdqo-content">
            <div class="pdqo-section pdqo-group">
                <div class="pdqo-col pdqo-span-12-of-12">
                    <div id="pdqo-f">
                        <div class="pdqo-section pdqo-group">
                            <div  class="pdqo-col pdqo-span-4-of-12">
                                <label style="display: none" for="pdqo-n"><?php if($fields['name']['required']) { ?><i
                                            class="pdqo-required form-1-pol-222">*</i><?php } ?> <?=$t['name'];?></label>
                                <input name="pdqo-n-field" id="pdqo-n" class="pdqo-field" type="text"
                                       placeholder="Ваше имя"
                                       value="<?=$fields['name']['value'];?>"/>
                            </div>
                            <div class="pdqo-col pdqo-span-4-of-12">
                                <label style="display: none" for="pdqo-p"><?php if($fields['phone']['required']) { ?><i class="pdqo-required">*</i><?php } ?> <?=$t['phone'];?>
                                </label>
                                <input name="pdqo-p-field" id="pdqo-p" class="pdqo-field" type="text"
                                       placeholder="Ваш телефон"
                                       value="<?=$fields['phone']['value'];?>"/>
                            </div>
                            <div class="pdqo-section pdqo-group">
                                <div class="pdqo-col pdqo-span-4-of-12">
                                    <button class="pdqo-button pdqo-confirm"><?=$t['checkout'];?></button>
                                </div>
                            </div>
                        </div>
                        <?php if($fields['email']['status']) { ?>
                        <div class="pdqo-section pdqo-group">
                            <div class="pdqo-col pdqo-span-12-of-12">
                                <label for="pdqo-e"><?php if($fields['email']['required']) { ?><i class="pdqo-required">*</i><?php } ?> <?=$t['email'];?>
                                </label>
                                <input name="pdqo-e-field" id="pdqo-e" class="pdqo-field" type="text"
                                       placeholder="<?=$fields['email']['placeholder'];?>"
                                       value="<?=$fields['email']['value'];?>"/>
                            </div>
                        </div>
                        <?php } ?>
                        <?php if($fields['comment']['status']) { ?>
                        <div class="pdqo-section pdqo-group">
                            <div class="pdqo-col pdqo-span-12-of-12">
                                <label for="pdqo-c"><?php if($fields['comment']['required']) { ?><i
                                            class="pdqo-required">*</i><?php } ?> <?=$t['comment'];?></label>
                                <textarea name="pdqo-c-field" id="pdqo-c" class="pdqo-field" type="text" rows="3"
                                          placeholder="<?=$fields['comment']['placeholder'];?>"></textarea>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                    <div class="pdqo-section pdqo-group" style="display: none">
                        <div class="pdqo-col pdqo-span-12-of-12">
                            <table class="pdqo-products"></table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?=$js;?>