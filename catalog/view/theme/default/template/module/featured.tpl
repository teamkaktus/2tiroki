<div class="hidden-lg hidden-md hidden-sm">
  <div class="recomended-carousel owl-carousel">
    <?php foreach ($products as $product) { ?>
    <div class="item">
      <img src="<?php echo $product['thumb']; ?>" class="img-responsive center-block target<?php echo $product['product_id']; ?>" alt="">
      <div class="item-wrap">
        <a href="<?php echo $product['href']; ?>" class="button read-more">Подробнее</a>
        <h2 class="product-title"><?php echo $product['name']; ?></h2>
        <div class="clearfix"></div>
        <div class="price-block">
          <?php if ($product['price']) { ?>
          <?php if (!$product['special']) { ?>
          <span class="price"><?php echo $product['price']; ?></span>
          <?php } else { ?>
          <span class="price"><?php echo $product['price']; ?></span>
          <?php } ?>
          <?php } ?>
          <div class="buttons-block">
            <a onclick="cart.add('<?php echo $product['product_id']; ?>');"
               class="button cart-btn to-cart" for="<?php echo $product['product_id']; ?>">В корзину</a>
            <a data-pdqo-item="<?php echo $product['product_id']; ?>" onclick="pdqoObject(this);"
               class="button click-btn">Купить<br>в один клик</a>
          </div>
        </div>
      </div>
    </div>
    <?php } ?>
  </div>
</div>
