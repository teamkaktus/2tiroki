<div class="hidden-xs">
    <div class="row-title hidden-xs">Популярные товары</div>
    <div class="recomended-carousel owl-carousel2">
        <?php foreach ($products as $product) { ?>
        <div class="item">
            <img src="<?php echo $product['thumb']; ?>" class="img-responsive center-block" alt="">
            <div class="item-wrap">
                <a href="<?php echo $product['href']; ?>" class="button read-more">Подробнее</a>
                <h2 class="product-title"><?php echo $product['name']; ?></h2>
                <div class="clearfix"></div>
                <div class="price-block">
                    <?php if ($product['price']) { ?>
                    <?php if (!$product['special']) { ?>
                    <span class="price"><?php echo $product['price']; ?></span>
                    <?php } else { ?>
                    <span class="price"><?php echo $product['price']; ?></span>
                    <?php } ?>
                    <?php } ?>
                    <div class="buttons-block">
                        <a onclick="cart.add('<?php echo $product['product_id']; ?>');"
                           class="button cart-btn">В корзину</a>
                        <a data-pdqo-item="<?php echo $product['product_id']; ?>" onclick="pdqoObject(this);"
                           class="button click-btn">Купить<br>в один клик</a>
                    </div>
                </div>
            </div>
        </div>
        <?php } ?>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $('.owl-carousel2').owlCarousel({
            loop: false,
            margin: 10,
            nav: false,
            responsive: {
                0: {
                    items: 1,
                    dots: false
                },
                450: {
                    items: 2,
                    dots: false
                },
                768: {
                    items: 3,
                    dots: false
                },
                992: {
                    items: 3,
                    dots: false
                },
                1200: {
                    items: 3,
                    dots: false
                }
            }
        })
    });
</script>