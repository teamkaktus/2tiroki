<?php echo $header; ?>
<div class="page">
    <section id="page-title" style="background-image: url(image/catalog/bg-large.jpg);">
        <div class="container">
            <div class="row">
                <h3>cпециализированный магазин</h3>
                <h2>Умных часов для детей</h2>
            </div>
        </div>
    </section>
    <section id="breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <Ul>
                        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                        <?php if($breadcrumb == end($breadcrumbs)) { ?>
                        <li><?php echo $breadcrumb['text']; ?></li>
                        <?php } else { ?>
                        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                        <?php } ?>
                        <?php } ?>
                    </Ul>
                </div>
            </div>
        </div>
    </section>
    <div class="content">
        <div class="container">
            <h2 class="h2-page-title"><?php echo $heading_title; ?></h2>
            <?php echo $content_top; ?>
            <div class="row">
                <div class="col-md-6">
                    <?php if ($thumb || $images) { ?>
                    <div class="product-gallery fotorama visible-md visible-lg visible-sm" data-nav="thumbs"
                         data-thumbwidth="86" data-thumbheight="86" data-thumbborderwidth="1" data-fit="cover">
                        <?php if ($thumb) { ?>
                        <img class="img-responsive" src="<?php echo $popup; ?>" alt="">
                        <?php } ?>
                        <?php if ($images) { ?>
                        <?php foreach ($images as $image) { ?>
                        <img class="img-responsive" src="<?php echo $image['popup']; ?>" alt="">
                        <?php } ?>
                        <?php } ?>
                    </div>
                    <?php } ?>
                </div>
              <div class="col-md-6">
                    <?php if ($thumb || $images) { ?>
                    <div class="owl-carousel product-owl-gallery visible-xs">
                        <?php if ($thumb) { ?>
                        <img class="img-responsive" src="<?php echo $popup; ?>" alt="">
                        <?php } ?>
                        <?php if ($images) { ?>
                        <?php foreach ($images as $image) { ?>
                        <img class="img-responsive" src="<?php echo $image['popup']; ?>" alt="">
                        <?php } ?>
                        <?php } ?>
                    </div>
                    <?php } ?>
                </div>
              
              
                <div class="col-md-6">
                    <span class="nalich"><?php //echo $text_stock; ?> <?php echo $stock; ?></span>
                    <?php if ($price) { ?>
                    <span class="price-small visible-xs"><?php echo $price; ?></span>
                    <div class="price-quant">
                        <h3 class="price hidden-xs"><?php echo $price; ?></h3>
                        <div id="abvv_product" class="quant-form">
                            <label for="input-quantity">Количество</label>
                            <input id="quant" type="text" name="quantity" value="1"
                                   id="input-quantity"/>
                            <span class="value plus-val">+</span>
                            <span class="value minus-val">-</span>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <?php } ?>
                    <div class="buttons-buy">
                        <a id="product">
                            <input type="hidden" name="product_id" value="<?php echo $product_id; ?>"/>
                            <a id="button-cart"
                               class="button cart-buy-btn blue-bg no-shadow">В корзину</a>
                        </a>
                        <a data-pdqo-item="<?php echo $product_id; ?>" onclick="pdqoObject(this);" class="button click-buy-btn no-shadow">Купить в один клик</a>
                    </div>
                    <div class="product-tabs">
                        <div class="tab">

                            <ul class="tabs">
                                <li><a href="#">Описание</a></li>
                                <li><a href="#">Характеристики</a></li>
                            </ul> <!-- / tabs -->

                            <div class="tab_content">
                                <div class="tabs_item"><?php echo $description; ?>
                                </div> <!-- / tabs_item -->
                                <div class="tabs_item">
                                    <?php echo $short_description; ?>
                                </div> <!-- / tabs_item -->
                            </div> <!-- / tab_content -->
                        </div> <!-- / tab -->
                    </div>
                </div>
            </div>
            <?php echo $column_right; ?></div>
    </div>
    <section class="common-cont">
        <div class="container">
            <div class="row">
                <div class="com-cont">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="com-wrap com-wrap___style_text">
                                <img src="image/catalog/del.png" alt="">
                                <span>Доставка по России 3-5 дней</span>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="com-wrap com-wrap___style_text">
                                <img src="image/catalog/cont.png" alt="">
                                <span>Каждые часы проверяются перед отправкой</span>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="com-wrap com-wrap___style_text">
                                <img src="image/catalog/polu.png" alt="">
                                <span>Оплата при получении</span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 col-md-offset-2">
                            <div class="com-wrap com-wrap___style_text">
                                <img src="image/catalog/docs.png" class="img__product_1" alt="">
                                <span class="span__product_1">В комплекте все необходимые документы (накладная. кассовый чек, гарантийный талон)</span>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="com-wrap com-wrap___style_text">
                                <img src="image/catalog/deliv7.png" class="img__product_1" alt="">
                                <span class="span__product_1">Более 800 пунктов выдачи заказов по всей России</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="recomended owl-overflov-hidden">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2><?php echo $text_related; ?></h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <?php echo $content_bottom; ?>
                </div>
            </div>
        </div>
    </section>
    <section id="feedbacks">
        <div class="container">
            <h2>Отзывы счастливых покупателей</h2>
            <div class="row">
                <div class="col-md-12">
                    <div class="feeds-wrap owl-carousel hidden-xs">
                        <div class="feed">
                            <div class="lazyYT" data-youtube-id="hnB5aGW9AaU" data-ratio="16:9">1</div>
                        </div>
                        <div class="feed">
                            <div class="lazyYT" data-youtube-id="QeXberdrtws" data-ratio="16:9">1</div>
                        </div>
                        <div class="feed">
                            <div class="lazyYT" data-youtube-id="bNhxOKF8gys" data-ratio="16:9">1</div>
                        </div>
                        <div class="feed">
                            <div class="lazyYT" data-youtube-id="wxHz2mWs_ZI" data-ratio="16:9">1</div>
                        </div>
                        <div class="feed">
                            <div class="lazyYT" data-youtube-id="1RwmGahvubA" data-ratio="16:9">1</div>
                        </div>
                        <div class="feed">
                            <div class="lazyYT" data-youtube-id="qmbFuLLc-Mg" data-ratio="16:9">1</div>
                        </div>
                        <div class="feed">
                            <div class="lazyYT" data-youtube-id="vm92lJtNi0A" data-ratio="16:9">1</div>
                        </div>
                    </div>

                    <div class="feeds-accordion visible-xs">
                        <div class="feed">
                            <div class="lazyYT" data-youtube-id="hnB5aGW9AaU" data-ratio="16:9">1</div>
                        </div>
                        <div class="feed-content">
                            <div class="feed">
                                <div class="lazyYT" data-youtube-id="QeXberdrtws" data-ratio="16:9">1</div>
                            </div>
                            <div class="feed">
                                <div class="lazyYT" data-youtube-id="bNhxOKF8gys" data-ratio="16:9">1</div>
                            </div>
                            <div class="feed">
                                <div class="lazyYT" data-youtube-id="wxHz2mWs_ZI" data-ratio="16:9">1</div>
                            </div>
                            <div class="feed">
                                <div class="lazyYT" data-youtube-id="1RwmGahvubA" data-ratio="16:9">1</div>
                            </div>
                            <div class="feed">
                                <div class="lazyYT" data-youtube-id="qmbFuLLc-Mg" data-ratio="16:9">1</div>
                            </div>
                            <div class="feed">
                                <div class="lazyYT" data-youtube-id="vm92lJtNi0A" data-ratio="16:9">1</div>
                            </div>
                        </div>
                        <a href="#" class="all-feeds button center-block visible-xs">Все отзывы</a>
                    </div>

                    <div class="feeds-nav hidden-xs">
                        <a href="#" class="feeds-btn feeds-prev"><i class="fa fa-chevron-left"></i></a>
                        <a href="#" class="feeds-btn feeds-next"><i class="fa fa-chevron-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script type="text/javascript"><!--
    $('.minus-val').click(function () {
        var $input = $(this).parent().find('input');
        var count = parseInt($input.val()) - 1;
        count = count < 1 ? 1 : count;
        $input.val(count);
        $input.change();
        var $sbm = $(this).parent().find('button');
        $sbm.click();
        return false;
    });
    $('.plus-val').click(function () {
        var $input = $(this).parent().find('input');
        $input.val(parseInt($input.val()) + 1);
        $input.change();
        var $sbm = $(this).parent().find('button');
        $sbm.click();
        return false;
    });
    //--></script>
<script type="text/javascript"><!--
    $('select[name=\'recurring_id\'], input[name="quantity"]').change(function () {
        $.ajax({
            url: 'index.php?route=product/product/getRecurringDescription',
            type: 'post',
            data: $('input[name=\'product_id\'], input[name=\'quantity\'], select[name=\'recurring_id\']'),
            dataType: 'json',
            beforeSend: function () {
                $('#recurring-description').html('');
            },
            success: function (json) {
                $('.alert, .text-danger').remove();

                if (json['success']) {
                    $('#recurring-description').html(json['success']);
                }
            }
        });
    });
    //--></script>
<script type="text/javascript"><!--
    $('#button-cart').on('click', function () {
        $.ajax({
            url: 'index.php?route=checkout/cart/add',
            type: 'post',
            data: $('#abvv_product input[type=\'text\'], #product input[type=\'hidden\'], #product input[type=\'radio\']:checked, #product input[type=\'checkbox\']:checked, #product select, #product textarea'),
            dataType: 'json',
            beforeSend: function () {
                $('#button-cart').button('loading');
            },
            complete: function () {
                $('#button-cart').button('reset');
            },
            success: function (json) {
                $('.alert, .text-danger').remove();
                $('.form-group').removeClass('has-error');

                if (json['error']) {
                    if (json['error']['option']) {
                        for (i in json['error']['option']) {
                            var element = $('#input-option' + i.replace('_', '-'));

                            if (element.parent().hasClass('input-group')) {
                                element.parent().after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
                            } else {
                                element.after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
                            }
                        }
                    }

                    if (json['error']['recurring']) {
                        $('select[name=\'recurring_id\']').after('<div class="text-danger">' + json['error']['recurring'] + '</div>');
                    }

                    // Highlight any found errors
                    $('.text-danger').parent().addClass('has-error');
                }

                if (json['success']) {
                    $('.breadcrumb').after('<div class="alert alert-success">' + json['success'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');

                    setTimeout(function () {
                        $('#cart > a').html('<img src="image/catalog/cart.png" alt=""><span>Корзина</span><span class="badge"> ' + json['total'] + '</span>');
                    }, 100);
                    setTimeout(function () {
                        $('.cart_mob_wee').html('<img src="image/catalog/cart-mob.png" class="img-responsive" alt=""><span class="badggee"> ' + json['total'] + '</span>');
                    }, 100);

                    $('html, body').animate({scrollTop: 0}, 'slow');

                    $('#cart > ul').load('index.php?route=common/cart/info ul li');
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    });
    //--></script>
<script type="text/javascript"><!--
    $('.date').datetimepicker({
        pickTime: false
    });

    $('.datetime').datetimepicker({
        pickDate: true,
        pickTime: true
    });

    $('.time').datetimepicker({
        pickDate: false
    });

    $('button[id^=\'button-upload\']').on('click', function () {
        var node = this;

        $('#form-upload').remove();

        $('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

        $('#form-upload input[name=\'file\']').trigger('click');

        if (typeof timer != 'undefined') {
            clearInterval(timer);
        }

        timer = setInterval(function () {
            if ($('#form-upload input[name=\'file\']').val() != '') {
                clearInterval(timer);

                $.ajax({
                    url: 'index.php?route=tool/upload',
                    type: 'post',
                    dataType: 'json',
                    data: new FormData($('#form-upload')[0]),
                    cache: false,
                    contentType: false,
                    processData: false,
                    beforeSend: function () {
                        $(node).button('loading');
                    },
                    complete: function () {
                        $(node).button('reset');
                    },
                    success: function (json) {
                        $('.text-danger').remove();

                        if (json['error']) {
                            $(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '</div>');
                        }

                        if (json['success']) {
                            alert(json['success']);

                            $(node).parent().find('input').attr('value', json['code']);
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                    }
                });
            }
        }, 500);
    });
    //--></script>
<script type="text/javascript"><!--
    $('#review').delegate('.pagination a', 'click', function (e) {
        e.preventDefault();

        $('#review').fadeOut('slow');

        $('#review').load(this.href);

        $('#review').fadeIn('slow');
    });

    $('#review').load('index.php?route=product/product/review&product_id=<?php echo $product_id; ?>');

    $('#button-review').on('click', function () {
        $.ajax({
            url: 'index.php?route=product/product/write&product_id=<?php echo $product_id; ?>',
            type: 'post',
            dataType: 'json',
            data: $("#form-review").serialize(),
            beforeSend: function () {
                $('#button-review').button('loading');
            },
            complete: function () {
                $('#button-review').button('reset');
            },
            success: function (json) {
                $('.alert-success, .alert-danger').remove();

                if (json['error']) {
                    $('#review').after('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
                }

                if (json['success']) {
                    $('#review').after('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');

                    $('input[name=\'name\']').val('');
                    $('textarea[name=\'text\']').val('');
                    $('input[name=\'rating\']:checked').prop('checked', false);
                }
            }
        });
    });

    $(document).ready(function () {
        $('.thumbnails').magnificPopup({
            type: 'image',
            delegate: 'a',
            gallery: {
                enabled: true
            }
        });
    });
    //--></script>
<?php echo $footer; ?>
