<?php echo $header; ?>
<section id="page-title" style="background-image: url(image/catalog/bg-large.jpg);">
  <div class="container">
    <div class="row">
      <h3>cпециализированный магазин</h3>
      <h2>Умных часов для детей</h2>
    </div>
  </div>
</section>
<section class="zakaz">
<div class="container">
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <h1 class="search-title-style"><?php echo $heading_title; ?></h1>
      <h2><?php echo $text_search; ?></h2>
      <?php if ($products) { ?>
      <br />
      <?php foreach ($products as $product) { ?>
      <div class="item it_styl_om">
        <img src="<?php echo $product['thumb']; ?>" class="img-responsive center-block" alt="">
        <div class="item-wrap">
          <a href="<?php echo $product['href']; ?>" class="button read-more">Подробнее</a>
          <h2 class="product-title"><?php echo $product['name']; ?></h2>
          <div class="clearfix"></div>
          <div class="price-block">
            <?php if ($product['price']) { ?>
            <?php if (!$product['special']) { ?>
            <span class="price"><?php echo $product['price']; ?></span>
            <?php } else { ?>
            <span class="price"><?php echo $product['price']; ?></span>
            <?php } ?>
            <?php } ?>
            <div class="buttons-block">
              <a href="#"
                 onclick="cart.add('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>');"
                 class="button cart-btn"><?php echo $button_cart; ?></a>
              <a data-pdqo-item="<?php echo $product['product_id']; ?>" onclick="pdqoObject(this);" class="button click-btn">Купить<br>в один клик</a>
            </div>
          </div>
        </div>
      </div>
      <?php } ?>
      <div class="row">
        <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
        <div class="col-sm-6 text-right"><?php echo $results; ?></div>
      </div>
      <?php } else { ?>
      <p><?php echo $text_empty; ?></p>
      <?php } ?>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
</section>
<script type="text/javascript"><!--
$('#button-search').bind('click', function() {
	url = 'index.php?route=product/search';

	var search = $('#content input[name=\'search\']').prop('value');

	if (search) {
		url += '&search=' + encodeURIComponent(search);
	}

	var category_id = $('#content select[name=\'category_id\']').prop('value');

	if (category_id > 0) {
		url += '&category_id=' + encodeURIComponent(category_id);
	}

	var sub_category = $('#content input[name=\'sub_category\']:checked').prop('value');

	if (sub_category) {
		url += '&sub_category=true';
	}

	var filter_description = $('#content input[name=\'description\']:checked').prop('value');

	if (filter_description) {
		url += '&description=true';
	}

	location = url;
});

$('#content input[name=\'search\']').bind('keydown', function(e) {
	if (e.keyCode == 13) {
		$('#button-search').trigger('click');
	}
});

$('select[name=\'category_id\']').on('change', function() {
	if (this.value == '0') {
		$('input[name=\'sub_category\']').prop('disabled', true);
	} else {
		$('input[name=\'sub_category\']').prop('disabled', false);
	}
});

$('select[name=\'category_id\']').trigger('change');
--></script>
<?php echo $footer; ?>