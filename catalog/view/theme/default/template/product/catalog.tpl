<?php echo $header; ?>
<section id="page-title" style="background-image: url(image/catalog/bg-large.jpg);">
    <div class="container">
        <div class="row">
            <h3>cпециализированный магазин</h3>
            <h2>Умных часов для детей</h2>
        </div>
    </div>
</section>
<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <?php echo $column_left; ?>
            </div>
            <div class="col-md-9">
                <section class="cont-title">
                    <h2><?php echo $heading_title; ?></h2>
                    <?php if ($description) { ?>
                    <?php echo $short_description2; ?>
                    <?php } ?>
                </section>
                <section class="content-section hidden-lg hidden-md hidden-sm">
                    <?php foreach ($categories as $category) { ?>
                    <div class="owl-carousel cat-carousel" style="display:block">
                        <?php if ($products[$category["category_id"]]) { ?>
                        <?php foreach ($products[$category["category_id"]] as $product) { ?>
                        <div class="item">
                            <img src="<?php echo $product['thumb']; ?>" class="img-responsive center-block" alt="">
                            <div class="item-wrap">
                                <a href="<?php echo $product['href']; ?>" class="button read-more">Подробнее</a>
                                <h2 class="product-title"><?php echo $product['name']; ?></h2>
                                <div class="clearfix"></div>
                                <div class="price-block">
                                    <?php if ($product['price']) { ?>
                                    <?php if (!$product['special']) { ?>
                                    <span class="price"><?php echo $product['price']; ?></span>
                                    <?php } else { ?>
                                    <span class="price"><?php echo $product['price']; ?></span>
                                    <?php } ?>
                                    <?php } ?>
                                    <div class="buttons-block">
                                        <a onclick="cart.add('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>');"
                                          class="button cart-btn to-cart" for="<?php echo $product['product_id']; ?>">В корзину</a>
                                        <a data-pdqo-item="<?php echo $product['product_id']; ?>" onclick="pdqoObject(this);"
                                          class="button click-btn">Купить<br>в один клик</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php } ?>
                        <?php } ?>
                    </div>
                    <?php } ?>
                </section>
                <section class="content-section hidden-xs">
                    <?php foreach ($categories as $category) { ?>
                        <?php if ($products[$category["category_id"]]) { ?>
                        <?php foreach ($products[$category["category_id"]] as $product) { ?>
                        <div class="item it_styl_om">
                            <img src="<?php echo $product['thumb']; ?>" class="img-responsive center-block target<?php echo $product['product_id']; ?>" alt="">
                            <div class="item-wrap">
                                <a href="<?php echo $product['href']; ?>" class="button read-more">Подробнее</a>
                                <h2 class="product-title"><?php echo $product['name']; ?></h2>
                                <div class="clearfix"></div>
                                <div class="price-block">
                                    <?php if ($product['price']) { ?>
                                    <?php if (!$product['special']) { ?>
                                    <span class="price"><?php echo $product['price']; ?></span>
                                    <?php } else { ?>
                                    <span class="price"><?php echo $product['price']; ?></span>
                                    <?php } ?>
                                    <?php } ?>
                                    <div class="buttons-block">
                                         <a onclick="cart.add('<?php echo $product['product_id']; ?>' , '<?php echo $product['minimum']; ?>');"
                                          class="button cart-btn to-cart" for="<?php echo $product['product_id']; ?>">В корзину</a>
                                         <a data-pdqo-item="<?php echo $product['product_id']; ?>" onclick="pdqoObject(this);"
                                          class="button click-btn">Купить<br>в один клик</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php } ?>
                        <?php } ?>
                    <?php } ?>
                </section>
                <p><?php echo $description; ?> </p>
                <?php echo $content_top; ?>
                <?php if (!$categories && !$products) { ?>
                <p><?php echo $text_empty; ?></p>
                <div class="buttons">
                    <div class="pull-right"><a href="<?php echo $continue; ?>"
                                               class="btn btn-primary"><?php echo $button_continue; ?></a></div>
                </div>
                <?php } ?>
                <?php echo $content_bottom; ?>
                <?php echo $column_right; ?>
            </div>
        </div>
    </div>
</div>
<section id="feedbacks">
  <div class="container">
    <h2>Отзывы счастливых покупателей</h2>
    <div class="row">
      <div class="col-md-12">
        <div class="feeds-wrap owl-carousel hidden-xs">
          <div class="feed">
            <div class="lazyYT" data-youtube-id="hnB5aGW9AaU" data-ratio="16:9">1</div>
          </div>
          <div class="feed">
            <div class="lazyYT" data-youtube-id="QeXberdrtws" data-ratio="16:9">1</div>
          </div>
          <div class="feed">
            <div class="lazyYT" data-youtube-id="bNhxOKF8gys" data-ratio="16:9">1</div>
          </div>
          <div class="feed">
            <div class="lazyYT" data-youtube-id="wxHz2mWs_ZI" data-ratio="16:9">1</div>
          </div>
          <div class="feed">
            <div class="lazyYT" data-youtube-id="1RwmGahvubA" data-ratio="16:9">1</div>
          </div>
          <div class="feed">
            <div class="lazyYT" data-youtube-id="qmbFuLLc-Mg" data-ratio="16:9">1</div>
          </div>
          <div class="feed">
            <div class="lazyYT" data-youtube-id="vm92lJtNi0A" data-ratio="16:9">1</div>
          </div>
        </div>

        <div class="feeds-accordion visible-xs">
          <div class="feed">
            <div class="lazyYT" data-youtube-id="hnB5aGW9AaU" data-ratio="16:9">1</div>
          </div>
          <div class="feed-content">
            <div class="feed">
              <div class="lazyYT" data-youtube-id="QeXberdrtws" data-ratio="16:9">1</div>
            </div>
            <div class="feed">
              <div class="lazyYT" data-youtube-id="bNhxOKF8gys" data-ratio="16:9">1</div>
            </div>
            <div class="feed">
              <div class="lazyYT" data-youtube-id="wxHz2mWs_ZI" data-ratio="16:9">1</div>
            </div>
            <div class="feed">
              <div class="lazyYT" data-youtube-id="1RwmGahvubA" data-ratio="16:9">1</div>
            </div>
            <div class="feed">
              <div class="lazyYT" data-youtube-id="qmbFuLLc-Mg" data-ratio="16:9">1</div>
            </div>
            <div class="feed">
              <div class="lazyYT" data-youtube-id="vm92lJtNi0A" data-ratio="16:9">1</div>
            </div>
          </div>
          <a href="#" class="all-feeds button center-block visible-xs">Все отзывы</a>
        </div>

        <div class="feeds-nav hidden-xs">
          <a href="#" class="feeds-btn feeds-prev"><i class="fa fa-chevron-left"></i></a>
          <a href="#" class="feeds-btn feeds-next"><i class="fa fa-chevron-right"></i></a>
        </div>
      </div>
    </div>
  </div>
</section>
<?php echo $footer; ?>





