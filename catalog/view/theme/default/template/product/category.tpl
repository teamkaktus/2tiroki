<?php echo $header; ?>
<section id="page-title" style="background-image: url(image/catalog/bg-large.jpg);">
    <div class="container">
        <div class="row">
            <h3>cпециализированный магазин</h3>
            <h2>Умных часов для детей</h2>
        </div>
    </div>
</section>
<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <?php echo $column_left; ?>
            </div>
            <div class="col-md-9">
                <section class="cont-title">
                    <h2><?php echo $heading_title; ?></h2>
                    <?php if ($thumb || $description) { ?>
                    <?php if ($description) { ?>
                    <?php echo $short_description2; ?>
                    <?php } ?>
                    <?php } ?>
                </section>
                <section class="content-section hidden-lg hidden-md hidden-sm">
                    <?php if ($products) { ?>
                    <div class="owl-carousel cat-carousel" style="display:block">
                        <?php foreach ($products as $product) { ?>
                        <div class="item">
                            <img src="<?php echo $product['thumb']; ?>" class="img-responsive center-block" alt="">
                            <div class="item-wrap">
                                <a href="<?php echo $product['href']; ?>" class="button read-more">Подробнее</a>
                                <h2 class="product-title"><?php echo $product['name']; ?></h2>
                                <div class="clearfix"></div>
                                <div class="price-block">
                                    <?php if ($product['price']) { ?>
                                    <?php if (!$product['special']) { ?>
                                    <span class="price"><?php echo $product['price']; ?></span>
                                    <?php } else { ?>
                                    <span class="price"><?php echo $product['price']; ?></span>
                                    <?php } ?>
                                    <?php } ?>
                                    <div class="buttons-block">
                                        <a href="#"
                                           onclick="cart.add('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>');"
                                           class="button cart-btn"><?php echo $button_cart; ?></a>
                                        <a data-pdqo-item="<?php echo $product['product_id']; ?>" onclick="pdqoObject(this);" class="button click-btn">Купить<br>в один клик</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                    <?php } ?>
                </section>
                <section class="content-section hidden-xs">
                    <?php if ($products) { ?>
                        <?php foreach ($products as $product) { ?>
                        <div class="item it_styl_om">
                            <img src="<?php echo $product['thumb']; ?>" class="img-responsive center-block" alt="">
                            <div class="item-wrap">
                                <a href="<?php echo $product['href']; ?>" class="button read-more">Подробнее</a>
                                <h2 class="product-title"><?php echo $product['name']; ?></h2>
                                <div class="clearfix"></div>
                                <div class="price-block">
                                    <?php if ($product['price']) { ?>
                                    <?php if (!$product['special']) { ?>
                                    <span class="price"><?php echo $product['price']; ?></span>
                                    <?php } else { ?>
                                    <span class="price"><?php echo $product['price']; ?></span>
                                    <?php } ?>
                                    <?php } ?>
                                    <div class="buttons-block">
                                        <a href="#"
                                           onclick="cart.add('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>');"
                                           class="button cart-btn"><?php echo $button_cart; ?></a>
                                        <a data-pdqo-item="<?php echo $product['product_id']; ?>" onclick="pdqoObject(this);" class="button click-btn">Купить<br>в один клик</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php } ?>
                    <?php } ?>
                </section>
                <p><?php echo $description; ?> </p>
                <?php echo $content_top; ?>
                <?php if (!$categories && !$products) { ?>
                <p><?php echo $text_empty; ?></p>
                <div class="buttons">
                    <div class="pull-right"><a href="<?php echo $continue; ?>"
                                               class="btn btn-primary"><?php echo $button_continue; ?></a></div>
                </div>
                <?php } ?>
                <?php echo $content_bottom; ?>
                <?php echo $column_right; ?>
            </div>
        </div>
    </div>
</div>
<?php echo $footer; ?>
