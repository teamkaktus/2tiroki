<?php echo $header; ?>
 <section id="breadcrumbs">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <Ul>
            <li><a href="">Главная</a></li>
            <li>Корзина</li>
          </Ul>
        </div>
      </div>
    </div>
  </section>
<section class="zakaz">
  <div class="container">
    <div class="row"><?php echo $column_left; ?>
      <?php if ($column_left && $column_right) { ?>
      <?php $class = 'col-sm-6'; ?>
      <?php } elseif ($column_left || $column_right) { ?>
      <?php $class = 'col-sm-9'; ?>
      <?php } else { ?>
      <?php $class = 'col-sm-12'; ?>
      <?php } ?>
      <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
        <h1 class="h2-page-title"><?php echo $heading_title; ?></h1>
        <p><?php echo $text_error; ?></p>
        <div class="buttons">
          <div class="pull-right"><a href="<?php echo $continue; ?>" style="padding-top: 15px" class="btn btn-primary check-btn zakaz-btn button"><?php echo $button_continue; ?></a></div>
        </div>
        <?php echo $content_bottom; ?></div>
      <?php echo $column_right; ?></div>
  </div>
</section>
<?php echo $footer; ?>