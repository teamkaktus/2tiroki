<?php
class ControllerCommonWholesalers extends Controller {
	public function index() {
       
          $this->document->addScript("https://cdn.jsdelivr.net/sweetalert2/6.0.1/sweetalert2.min.js");
          $this->document->addStyle("https://cdn.jsdelivr.net/sweetalert2/6.0.1/sweetalert2.min.css");
          
		$this->document->setTitle($this->config->get('config_meta_title'));
		$this->document->setDescription($this->config->get('config_meta_description'));
		$this->document->setKeywords($this->config->get('config_meta_keyword'));

		if (isset($this->request->get['route'])) {
			$this->document->addLink(HTTP_SERVER, 'canonical');
		}

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/wholesalers.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/common/wholesalers.tpl', $data));
		} else {
			$this->response->setOutput($this->load->view('default/template/common/wholesalers.tpl', $data));
		}
	}
     public function oContact(){
        $user_query = $this->db->query("SELECT email FROM " . DB_PREFIX . "user WHERE username = 'admin' AND (user_group_id = '1') AND status = '1'");

        if ($user_query->num_rows) {
            $admin_email = $user_query->row['email'];
        }
        $mailContent = '
            <table>
                <tr>
                    <td>Name</td>
                    <td>'.$_POST['oName'].'</td>
                </tr>
                <tr>
                    <td>Phone</td>
                    <td>'.$_POST['oPhone'].'</td>
                </tr>
            </table>
        ';

        $mail = new Mail();
        $mail->protocol = $this->config->get('config_mail_protocol');
        $mail->parameter = $this->config->get('config_mail_parameter');
        $mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
        $mail->smtp_username = $this->config->get('config_mail_smtp_username');
        $mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
        $mail->smtp_port = $this->config->get('config_mail_smtp_port');
        $mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

        $mail->setTo($admin_email);
        $mail->setFrom('no-reply@tiroki.net');
        $mail->setSender(html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));
        $mail->setSubject("Форма обратной связи");
        $mail->setHtml($mailContent);
        $mail->send();
    }
    public function opContact(){
        $user_query = $this->db->query("SELECT email FROM " . DB_PREFIX . "user WHERE username = 'admin' AND (user_group_id = '1') AND status = '1'");

        if ($user_query->num_rows) {
            $admin_email = $user_query->row['email'];
        }
        $mailContent = '
            <table>
                <tr>
                    <td>Name</td>
                    <td>'.$_POST['oName'].'</td>
                </tr>
                <tr>
                    <td>Phone</td>
                    <td>'.$_POST['oPhone'].'</td>
                </tr>
            </table>
        ';

        $mail = new Mail();
        $mail->protocol = $this->config->get('config_mail_protocol');
        $mail->parameter = $this->config->get('config_mail_parameter');
        $mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
        $mail->smtp_username = $this->config->get('config_mail_smtp_username');
        $mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
        $mail->smtp_port = $this->config->get('config_mail_smtp_port');
        $mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

        $mail->setTo($admin_email);
        $mail->setFrom('no-reply@tiroki.net');
        $mail->setSender(html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));
        $mail->setSubject("Интересуется оптом");
        $mail->setHtml($mailContent);
        $mail->send();
    }
}